package StoneClass;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;

/**
 * Created by sadiq on 9/8/2016.
 */
public class StoneOne {
    private GamePanel gamePanelStoneOne;
    private Bitmap bitmapStone;
    int x, y, dx;

    public StoneOne(GamePanel gamePanelStoneOne, Bitmap bitmapStone, int x, int y ){
        this.gamePanelStoneOne = gamePanelStoneOne;
        this.bitmapStone = bitmapStone;
        this.x = x;
        this.y = y;
    }

    public void drawS(Canvas canvas){
        //canvas.drawBitmap(this.bitmapStone, this.x, this.y, null);

        if ( x < 0){
            //canvas.drawBitmap(this.bitmapStone, this.x + GamePanel.WIDTH, this.y, null);
        }
    }

    public void update(){
        this.x += this.dx;

        if (this.x <- GamePanel.WIDTH){

            this.x = 0;
        }
    }


    public void setStoneOneVector(int dx){
        this.dx = dx;
    }

    public int getStoneOneHeight(){
        return this.bitmapStone.getHeight();
    }

    public int getStoneOneWidth(){
        return this.bitmapStone.getWidth();
    }

    public int getStoneOneX(){
        return this.x;
    }

    public int getStoneOneY(){
        return this.y;
    }

}
