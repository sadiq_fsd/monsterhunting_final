package BulletsClasses;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.Gun;

import CustomDialogBox.DialogBoxCustom;
import MySpriteSheet.HammerheadOneSprite;
import MySpriteSheet.HammerheadSprite;
import MySpriteSheet.SharkSmallSprite;
import MySpriteSheet.Sprite;

/**
 * Created by sadiq on 11/29/2016.
 */
public class BulletEleven {
    private boolean resetBullet = true; //for bullet Reset

    private GamePanel gamePanelViewEleven;
    private Bitmap bulletElevenBitMap;
    private int x = 0;
    private int y = 0;

    private int currentXPosition = 0;
    private int currentYPosition = 0;

    private Sprite sprite;
    private Gun gun;

    private HammerheadOneSprite hammerheadOneSprite;
    private SharkSmallSprite sharkSmallSprite;
    //private SmallAnglarFishSprite smallAnglarFishSprite;
    private HammerheadSprite hammerheadSprite;

    private boolean bulletDirectionBool = true;

    public BulletEleven(GamePanel gamePanel, Bitmap bulletElevenBitMap, int x, int y, Sprite sprite, Gun gun,
                        HammerheadOneSprite hammerheadOneSprite, SharkSmallSprite sharkSmallSprite,
                        HammerheadSprite hammerheadSprite ){ //SmallAnglarFishSprite smallAnglarFishSprite,

        this.gamePanelViewEleven = gamePanel;
        this.bulletElevenBitMap = bulletElevenBitMap;
        this.x = x;
        this.y = y;
        this.currentXPosition = gun.getGunX() + gun.getDiverWidthSpriteSheet();
        this.currentYPosition = (int) (gun.getGunY() + (gun.getDiverHeightSpriteSheet()/ 2.9));
        this.hammerheadOneSprite = hammerheadOneSprite;
        this.sharkSmallSprite = sharkSmallSprite;
        //this.smallAnglarFishSprite = smallAnglarFishSprite;
        this.hammerheadSprite = hammerheadSprite;
        this.sprite = sprite;
        this.gun = gun;
    }

    public void onDraw(Canvas canvas){

        this.updateBullet();
        canvas.drawBitmap(this.bulletElevenBitMap, this.currentXPosition - (int) (gun.getDiverWidthSpriteSheet()/31.2), this.currentYPosition + (int) (gun.getDiverHeightSpriteSheet()/ 4.4), null);
    }

    /**
     * Update the state of the sprite.
     */
    private void updateBullet(){

        if(GamePanel.isFired){
            if (bulletDirectionBool) {
                setY(this.gun.getGunY()+ (int) (gun.getDiverHeightSpriteSheet()/1.956));//45
                setCurrentYPosition(this.gun.getGunY() + (int) (gun.getDiverHeightSpriteSheet()/4.4));//20
                bulletDirectionBool = false;
                GamePanel.totalChance += -1;
            }

            if ( this.currentXPosition < GamePanel.WIDTH && GamePanel.isFired){

                //this.currentXPosition += 20;
                this.currentXPosition += (GamePanel.WIDTH/59.8);

                isCollide();
                if ( this.currentXPosition >= ( GamePanel.WIDTH ) && this.currentXPosition <= ( this.currentXPosition + 22 ) ){

                    this.currentXPosition = GamePanel.WIDTH;
                }

                if (this.currentXPosition == GamePanel.WIDTH && GamePanel.bulletCount == 11 ){

                    this.resetBullet = false; //for bullet Reset
                    GamePanel.isFired = false;//=setBulletFired(false);
                    this.bulletDirectionBool = true;
                    //GamePanel.bulletCount = 0;
                    GamePanel.bulletEleven = null;
                }
            }
        }
        else if (this.currentXPosition == GamePanel.WIDTH && !GamePanel.isFired && !this.resetBullet ) {
            this.currentXPosition = 0;
            GamePanel.isFired = false;//=setBulletFired(false);
        }
    }

    private void isCollide(){
        //sprite sheet
        if (this.currentXPosition > (sprite.getSpriteSheetX()+(sprite.getWidthSpriteSheet()/4))
                && this.currentXPosition <= (sprite.getSpriteSheetX() + sprite.getWidthSpriteSheet())
                && this.y >= (this.sprite.getSpriteSheeRandomY())
                && this.y <= (this.sprite.getSpriteSheeRandomY() + this.sprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isEnemyAndFireCollision();
        }


        if (this.currentXPosition > (sharkSmallSprite.getSpriteSheetX()+ (sharkSmallSprite.getWidthSpriteSheet()/4))
                && this.currentXPosition <= (sharkSmallSprite.getSpriteSheetX() + sharkSmallSprite.getWidthSpriteSheet())
                && this.y >= (this.sharkSmallSprite.getSpriteSheeRandomY())
                && this.y <= (this.sharkSmallSprite.getSpriteSheeRandomY() + this.sharkSmallSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isSmallSharkSpriteAndFireCollision();
        }

        if (this.currentXPosition > (hammerheadOneSprite.getSpriteSheetX()+ (hammerheadSprite.getWidthSpriteSheet()/4))
                && this.currentXPosition <= (hammerheadOneSprite.getSpriteSheetX() + hammerheadOneSprite.getWidthSpriteSheet())
                && this.y >= (this.hammerheadOneSprite.getSpriteSheeRandomY())
                && this.y <= (this.hammerheadOneSprite.getSpriteSheeRandomY() + this.hammerheadOneSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isHammerheadOneSpriteAndFireCollision();
        }


        if (this.currentXPosition > (hammerheadSprite.getSpriteSheetX()+ (hammerheadSprite.getWidthSpriteSheet()/4))
                && this.currentXPosition <= (hammerheadSprite.getSpriteSheetX() + hammerheadSprite.getWidthSpriteSheet())
                && this.y >= (this.hammerheadSprite.getSpriteSheeRandomY())
                && this.y <= (this.hammerheadSprite.getSpriteSheeRandomY() + this.hammerheadSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isHammerheadSpriteAndFireCollision();
        }

        /*if (this.currentXPosition > (smallAnglarFishSprite.getSpriteSheetX()+(smallAnglarFishSprite.getWidthSpriteSheet()/4) )
                && this.currentXPosition <= (smallAnglarFishSprite.getSpriteSheetX() + smallAnglarFishSprite.getWidthSpriteSheet())
                && this.y >= (this.smallAnglarFishSprite.getSpriteSheeRandomY())
                && this.y <= (this.smallAnglarFishSprite.getSpriteSheeRandomY() + this.smallAnglarFishSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isSmallAnglarFishSpriteAndFireCollision();
        }*/

        if (GamePanel.totalChance == 0){ //for showing dialog box
            DialogBoxCustom.isGameOver = true;
        }
    }

    private void isEnemyAndFireCollision(){
        this.currentXPosition = GamePanel.WIDTH;//1024;
        GamePanel.scoreValue += 5;

        GamePanel.killAllEnemyFireCount += 1;
        if ( GamePanel.killAllEnemyFireCount == 10){
            this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killAllEnemyFireCount = 0;
            this.sprite.rYValue = this.gamePanelViewEleven.getRandomValue();
        }
    }

    private void isSmallSharkSpriteAndFireCollision(){
        this.currentXPosition = GamePanel.WIDTH;//1196;
        GamePanel.scoreValue += 5;
        GamePanel.killSmallSharkFireCount += 1;
        if ( GamePanel.killSmallSharkFireCount == 3){
            this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killSmallSharkFireCount = 0;
            this.sharkSmallSprite.rYValue = this.gamePanelViewEleven.getRandomValue();
        }
    }

    private void isHammerheadOneSpriteAndFireCollision(){
        this.currentXPosition = GamePanel.WIDTH;//1196;
        GamePanel.scoreValue += 5;

        GamePanel.killHammerHeadOneFireCount += 1;
        if ( GamePanel.killHammerHeadOneFireCount == 5){
            this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killHammerHeadOneFireCount = 0;
            this.hammerheadOneSprite.rYValue = this.gamePanelViewEleven.getRandomValue();
        }
    }

    private void isHammerheadSpriteAndFireCollision(){
        this.currentXPosition = GamePanel.WIDTH;//1196
        GamePanel.scoreValue += 5;

        GamePanel.killHammerHeadFireCount += 1;
        if ( GamePanel.killHammerHeadFireCount == 3){
            this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killHammerHeadFireCount = 0;
            this.hammerheadSprite.rYValue = this.gamePanelViewEleven.getRandomValue();
        }
    }

    /*private void isSmallAnglarFishSpriteAndFireCollision(){
        this.currentXPosition = GamePanel.WIDTH;//1196;
        GamePanel.scoreValue += 5;

        GamePanel.killAngularFireCount += 1;
        if ( GamePanel.killAngularFireCount == 3){
            this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killAngularFireCount = 0;
            this.smallAnglarFishSprite.rYValue = this.gamePanelViewEleven.getRandomValue();
        }
    }*/

    private void setY(int y){
        this.y = y;
    }

    public void setCurrentYPosition(int currentYPosition){
        this.currentYPosition = currentYPosition;
    }
}
