package BulletsClasses;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.Gun;

import CustomDialogBox.DialogBoxCustom;
import MySpriteSheet.HammerheadOneSprite;
import MySpriteSheet.HammerheadSprite;
import MySpriteSheet.SharkSmallSprite;
import MySpriteSheet.Sprite;

/**
 * Created by sadiq on 9/7/2016.
 */
public class BulletEight {
    private boolean resetBullet = true; //for bullet Reset

    private GamePanel gamePanelViewEight;
    private Bitmap bulletEightBitMap;
    private int x = 0;
    private int y = 0;
    //private TargetBirdOne targetBirdOne;
    //private TargetBird targetBird;

    private int currentXPosition = 0;
    private int currentYPosition = 0;

    private Sprite sprite;
    private Gun gun;

    private HammerheadOneSprite hammerheadOneSprite;
    private SharkSmallSprite sharkSmallSprite;
    //private SmallAnglarFishSprite smallAnglarFishSprite;
    private HammerheadSprite hammerheadSprite;

    private boolean bulletDirectionBool = true;

    //public BulletEight(GamePanel gamePanel, Bitmap bulletEightBitMap, int x, int y, TargetBird targetBird, TargetBirdOne targetBirdOne, Sprite sprite, Gun gun ){
    public BulletEight(GamePanel gamePanel, Bitmap bulletEightBitMap, int x, int y, Sprite sprite, Gun gun,
                       HammerheadOneSprite hammerheadOneSprite, SharkSmallSprite sharkSmallSprite,
                        HammerheadSprite hammerheadSprite ){ //SmallAnglarFishSprite smallAnglarFishSprite,

        this.gamePanelViewEight = gamePanel;
        this.bulletEightBitMap = bulletEightBitMap;
        this.x = x;
        this.y = y;
        //this.targetBirdOne = targetBirdOne;
        //this.targetBird = targetBird;
        this.currentXPosition = gun.getGunX() + gun.getDiverWidthSpriteSheet();
        //this.currentYPosition = gun.getGunY() + 30;
        this.currentYPosition = (int) (gun.getGunY() + (gun.getDiverHeightSpriteSheet()/ 2.9));
        this.hammerheadOneSprite = hammerheadOneSprite;
        this.sharkSmallSprite = sharkSmallSprite;
        //this.smallAnglarFishSprite = smallAnglarFishSprite;
        this.hammerheadSprite = hammerheadSprite;
        this.sprite = sprite;
        this.gun = gun;
    }

    public void onDraw(Canvas canvas){
        this.updateBullet();
        //canvas.drawBitmap(this.bulletEightBitMap, this.x, this.y, null);

        //canvas.drawBitmap(this.bulletEightBitMap, this.currentXPosition -10, this.currentYPosition + 20, null);
        canvas.drawBitmap(this.bulletEightBitMap, this.currentXPosition - (int) (gun.getDiverWidthSpriteSheet()/31.2), this.currentYPosition + (int) (gun.getDiverHeightSpriteSheet()/ 4.4), null);
    }

    /**
     * Update the state of the sprite.
     */
    private void updateBullet(){


        if(GamePanel.isFired){

            if (bulletDirectionBool) {
                setY(this.gun.getGunY()+ (int) (gun.getDiverHeightSpriteSheet()/1.956));//45
                setCurrentYPosition(this.gun.getGunY() + (int) (gun.getDiverHeightSpriteSheet()/4.4));//20
                bulletDirectionBool = false;

                GamePanel.totalChance += -1;
            }

            //if ( this.x < this.gamePanelViewEight.getBackgroundTotalWidth() && GamePanel.isFired){
            if ( this.currentXPosition < GamePanel.WIDTH && GamePanel.isFired){
                //this.currentXPosition += 20;
                this.currentXPosition += (GamePanel.WIDTH/59.8);

                isCollide();
                //isCollision();
                //if ( this.x == ( this.gamePanelViewEight.getBackgroundTotalWidth() - 4) ){
                //if ( this.currentXPosition == ( GamePanel.WIDTH + 4) ){
                //if ( this.currentXPosition >= ( GamePanel.WIDTH ) && this.currentXPosition <= ( 1218 ) ){
                if ( this.currentXPosition >= ( GamePanel.WIDTH ) && this.currentXPosition <= ( this.currentXPosition + 22 ) ){
                    //this.x = this.gamePanelViewEight.getBackgroundTotalWidth();
                    this.currentXPosition = GamePanel.WIDTH;
                }

                //if (this.x == this.gamePanelViewEight.getBackgroundTotalWidth() && GamePanel.bulletCount == 8 ){ //not in use
                //if (this.currentXPosition == GamePanel.WIDTH && GamePanel.bulletCount >= 8 ){//in use
                if (this.currentXPosition == GamePanel.WIDTH && GamePanel.bulletCount == 8 ){//in use
                    this.resetBullet = false; //for bullet Reset
                    GamePanel.isFired = false;//=setBulletFired(false);
                    this.bulletDirectionBool = true;
                    //GamePanel.bulletCount = 0;
                    GamePanel.bulletEight = null;
                }
            }
        }
        //else if (this.x == this.gamePanelViewEight.getBackgroundTotalWidth() && !GamePanel.isFired && !this.resetBullet ) {
        else if (this.currentXPosition == GamePanel.WIDTH && !GamePanel.isFired && !this.resetBullet ) {
            this.currentXPosition = 0;
            GamePanel.isFired = false;//=setBulletFired(false);

        }
    }

    /*private void isCollide(){
        if ( this.x  >= this.targetBird.getX()
                && this.y >= (this.targetBird.getY() )
                && this.y <= (this.targetBird.getY() + this.targetBird.getHeight())
                ){
            isCollision();

        } else if ( this.x  >= this.targetBirdOne.getX()
                && this.y >= (this.targetBirdOne.getY() )
                && this.y <= (this.targetBirdOne.getY() + this.targetBirdOne.getHeight())
                ){
            isCollisionOne();

        }
    }*/

    private void isCollide(){
        //sprite sheet
        if (this.currentXPosition > (sprite.getSpriteSheetX()+(sprite.getWidthSpriteSheet()/4))
                && this.currentXPosition <= (sprite.getSpriteSheetX() + sprite.getWidthSpriteSheet())
                && this.y >= (this.sprite.getSpriteSheeRandomY())
                && this.y <= (this.sprite.getSpriteSheeRandomY() + this.sprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isEnemyAndFireCollision();
        }


        if (this.currentXPosition > (sharkSmallSprite.getSpriteSheetX()+ (sharkSmallSprite.getWidthSpriteSheet()/4))
                && this.currentXPosition <= (sharkSmallSprite.getSpriteSheetX() + sharkSmallSprite.getWidthSpriteSheet())
                && this.y >= (this.sharkSmallSprite.getSpriteSheeRandomY())
                && this.y <= (this.sharkSmallSprite.getSpriteSheeRandomY() + this.sharkSmallSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isSmallSharkSpriteAndFireCollision();
        }

        if (this.currentXPosition > (hammerheadOneSprite.getSpriteSheetX()+ (hammerheadSprite.getWidthSpriteSheet()/4))
                && this.currentXPosition <= (hammerheadOneSprite.getSpriteSheetX() + hammerheadOneSprite.getWidthSpriteSheet())
                && this.y >= (this.hammerheadOneSprite.getSpriteSheeRandomY())
                && this.y <= (this.hammerheadOneSprite.getSpriteSheeRandomY() + this.hammerheadOneSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isHammerheadOneSpriteAndFireCollision();
        }


        if (this.currentXPosition > (hammerheadSprite.getSpriteSheetX()+ (hammerheadSprite.getWidthSpriteSheet()/4))
                && this.currentXPosition <= (hammerheadSprite.getSpriteSheetX() + hammerheadSprite.getWidthSpriteSheet())
                && this.y >= (this.hammerheadSprite.getSpriteSheeRandomY())
                && this.y <= (this.hammerheadSprite.getSpriteSheeRandomY() + this.hammerheadSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isHammerheadSpriteAndFireCollision();
        }

        /*if (this.currentXPosition > (smallAnglarFishSprite.getSpriteSheetX()+(smallAnglarFishSprite.getWidthSpriteSheet()/4) )
                && this.currentXPosition <= (smallAnglarFishSprite.getSpriteSheetX() + smallAnglarFishSprite.getWidthSpriteSheet())
                && this.y >= (this.smallAnglarFishSprite.getSpriteSheeRandomY())
                && this.y <= (this.smallAnglarFishSprite.getSpriteSheeRandomY() + this.smallAnglarFishSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isSmallAnglarFishSpriteAndFireCollision();
        }*/

        if (GamePanel.totalChance == 0){ //for showing dialog box
            DialogBoxCustom.isGameOver = true;
        }
    }

    /*private void isCollide(){
        *//*if ( this.x  >= this.targetBird.getX()
                && this.y >= (this.targetBird.getY() )
                && this.y <= (this.targetBird.getY() + this.targetBird.getHeight())
                ){
            isCollision();

        } else if ( this.x  >= this.targetBirdOne.getX()
                && this.y >= (this.targetBirdOne.getY() )
                && this.y <= (this.targetBirdOne.getY() + this.targetBirdOne.getHeight())
                ){
            isCollisionOne();

        }*//*
        //else if (this.x > sprite.getSpriteSheetX() ){ //enemy and fire collision
        //sprite sheet
        if (this.currentXPosition > sprite.getSpriteSheetX()
                && this.currentXPosition <= (sprite.getSpriteSheetX() + sprite.getWidthSpriteSheet())
                && this.y >= (this.sprite.getSpriteSheeRandomY())
                && this.y <= (this.sprite.getSpriteSheeRandomY() + this.sprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isEnemyAndFireCollision();
        }


        if (this.currentXPosition > sharkSmallSprite.getSpriteSheetX()
                && this.currentXPosition <= (sharkSmallSprite.getSpriteSheetX() + sharkSmallSprite.getWidthSpriteSheet())
                && this.y >= (this.sharkSmallSprite.getSpriteSheeRandomY())
                && this.y <= (this.sharkSmallSprite.getSpriteSheeRandomY() + this.sharkSmallSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isSmallSharkSpriteAndFireCollision();
        }

        if (this.currentXPosition > hammerheadOneSprite.getSpriteSheetX()
                && this.currentXPosition <= (hammerheadOneSprite.getSpriteSheetX() + hammerheadOneSprite.getWidthSpriteSheet())
                && this.y >= (this.hammerheadOneSprite.getSpriteSheeRandomY())
                && this.y <= (this.hammerheadOneSprite.getSpriteSheeRandomY() + this.hammerheadOneSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isHammerheadOneSpriteAndFireCollision();
        }


        if (this.currentXPosition > hammerheadSprite.getSpriteSheetX()
                && this.currentXPosition <= (hammerheadSprite.getSpriteSheetX() + hammerheadSprite.getWidthSpriteSheet())
                && this.y >= (this.hammerheadSprite.getSpriteSheeRandomY())
                && this.y <= (this.hammerheadSprite.getSpriteSheeRandomY() + this.hammerheadSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isHammerheadSpriteAndFireCollision();
        }

        if (this.currentXPosition > smallAnglarFishSprite.getSpriteSheetX()
                && this.currentXPosition <= (smallAnglarFishSprite.getSpriteSheetX() + smallAnglarFishSprite.getWidthSpriteSheet())
                && this.y >= (this.smallAnglarFishSprite.getSpriteSheeRandomY())
                && this.y <= (this.smallAnglarFishSprite.getSpriteSheeRandomY() + this.smallAnglarFishSprite.getHeightSpriteSheet() )  ){ //enemy and fire collision
            isSmallAnglarFishSpriteAndFireCollision();
        }
        *//**
         * this condition comment 10-3-2016(monday)
         *//*
        //else if (this.x > (this.gamePanelViewEight.getBackgroundTotalWidth()-10) && GamePanel.totalChance > 0 ){//&& !isCollide
        *//*else if (this.x > (GamePanel.WIDTH-10) && GamePanel.totalChance > 0 ){//&& !isCollide
            GamePanel.totalChance += -1;

        }*//*

        if (GamePanel.totalChance == 0){ //for showing dialog box
            DialogBoxCustom.isGameOver = true;
        }
    }*/

    private void isEnemyAndFireCollision(){
        //this.x = this.gamePanelViewEight.getBackgroundTotalWidth();//1024;
        this.currentXPosition = GamePanel.WIDTH;//1024;
        GamePanel.scoreValue += 5;

        GamePanel.killAllEnemyFireCount += 1;
        if ( GamePanel.killAllEnemyFireCount == 10){
            //this.sprite.setSpriteSheetX(this.gamePanelViewEight.getBackgroundTotalWidth() + 500 );
            this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killAllEnemyFireCount = 0;
            this.sprite.rYValue = this.gamePanelViewEight.getRandomValue();
        }
    }

    private void isSmallSharkSpriteAndFireCollision(){
        //this.x = this.view.getBackgroundTotalWidth();//1024;
        this.currentXPosition = GamePanel.WIDTH;//1196;

        GamePanel.scoreValue += 5;

        GamePanel.killSmallSharkFireCount += 1;
        if ( GamePanel.killSmallSharkFireCount == 3){
            //this.sprite.setSpriteSheetX(this.view.getBackgroundTotalWidth() + 500 );
            this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killSmallSharkFireCount = 0;
            this.sharkSmallSprite.rYValue = this.gamePanelViewEight.getRandomValue();
        }
    }

    private void isHammerheadOneSpriteAndFireCollision(){
        //this.x = this.view.getBackgroundTotalWidth();//1024;
        this.currentXPosition = GamePanel.WIDTH;//1196;

        GamePanel.scoreValue += 5;

        GamePanel.killHammerHeadOneFireCount += 1;
        if ( GamePanel.killHammerHeadOneFireCount == 5){
            //this.sprite.setSpriteSheetX(this.view.getBackgroundTotalWidth() + 500 );
            this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killHammerHeadOneFireCount = 0;
            this.hammerheadOneSprite.rYValue = this.gamePanelViewEight.getRandomValue();
        }
    }

    private void isHammerheadSpriteAndFireCollision(){
        //this.x = this.view.getBackgroundTotalWidth();//1024;
        this.currentXPosition = GamePanel.WIDTH;//1196;

        GamePanel.scoreValue += 5;

        GamePanel.killHammerHeadFireCount += 1;
        if ( GamePanel.killHammerHeadFireCount == 3){
            //this.sprite.setSpriteSheetX(this.view.getBackgroundTotalWidth() + 500 );
            this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killHammerHeadFireCount = 0;
            this.hammerheadSprite.rYValue = this.gamePanelViewEight.getRandomValue();
        }
    }

    /*private void isSmallAnglarFishSpriteAndFireCollision(){
        //this.x = this.view.getBackgroundTotalWidth();//1024;
        this.currentXPosition = GamePanel.WIDTH;//1196;

        GamePanel.scoreValue += 5;

        GamePanel.killAngularFireCount += 1;
        if ( GamePanel.killAngularFireCount == 3){
            //this.sprite.setSpriteSheetX(this.view.getBackgroundTotalWidth() + 500 );
            this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            GamePanel.killAngularFireCount = 0;
            this.smallAnglarFishSprite.rYValue = this.gamePanelViewEight.getRandomValue();
        }
    }*/

    /*private void isCollisionOne(){
        if ( this.x  >= this.targetBirdOne.getX()
                && this.y >= (this.targetBirdOne.getY() )
                && this.y <= (this.targetBirdOne.getY() + this.targetBirdOne.getHeight())
                ){

            //this.x = this.gamePanelViewEight.getBackgroundTotalWidth();//1024;
            this.x = GamePanel.WIDTH;//1024;
            GamePanel.scoreValue += 5;
            this.targetBirdOne.setY(520);
            this.targetBirdOne.targetBirdBoolean(true);

        }
        //else if (this.x > (this.gamePanelViewEight.getBackgroundTotalWidth()-10) && GamePanel.totalChance > 0 ){//&& !isCollide
        else if (this.x > (GamePanel.WIDTH-10) && GamePanel.totalChance > 0 ){//&& !isCollide
            GamePanel.totalChance += -1;
        }

        if (GamePanel.totalChance == 0){ //for showing dialog box
            DialogBoxCustom.isGameOver = true;
        }
    }

    private void isCollision(){
        if ( this.x  >= this.targetBird.getX()
                && this.y >= (this.targetBird.getY() )
                && this.y <= (this.targetBird.getY() + this.targetBird.getHeight())
                ){

            //this.x = this.gamePanelViewEight.getBackgroundTotalWidth();//1024;
            this.x = GamePanel.WIDTH;//1024;
            GamePanel.scoreValue += 5;
            this.targetBird.setY(0);
            this.targetBird.targetBirdBoolean(true);

        }
        //else if (this.x > (this.gamePanelViewEight.getBackgroundTotalWidth()-10) && GamePanel.totalChance > 0 ){//&& !isCollide
        else if (this.x > (GamePanel.WIDTH-10) && GamePanel.totalChance > 0 ){//&& !isCollide
            GamePanel.totalChance += -1;

            //isCollide = false;
        }

        if (GamePanel.totalChance == 0){ //for showing dialog box
            DialogBoxCustom.isGameOver = true;

        }
    }*/

    private void setY(int y){
        this.y = y;
    }

    public void setCurrentYPosition(int currentYPosition){
        this.currentYPosition = currentYPosition;
    }
}
