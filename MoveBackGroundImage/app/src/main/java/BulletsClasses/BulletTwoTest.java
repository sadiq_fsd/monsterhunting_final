package BulletsClasses;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.TargetBird;
import com.google.android.gms.example.movebackgroundimage.TargetBirdOne;

/**
 * Created by sadiq on 9/8/2016.
 */
public class BulletTwoTest extends BulletCommon {
    private int bulletTwoX = 0;

    public BulletTwoTest(GamePanel gamePanel, Bitmap bulletEightBitMap, int x, int y, TargetBird targetBird, TargetBirdOne targetBirdOne) {
        super(gamePanel, bulletEightBitMap, x, y, targetBird , targetBirdOne);

    }

    public void onDraw(Canvas canvas){
        updateBullet();
        canvas.drawBitmap(bulletBitMap, x, y, null);
    }

    /**
     * bullet X
     */
    public void setBulletTwoX(int x){
        this.bulletTwoX = x;
    }
}
