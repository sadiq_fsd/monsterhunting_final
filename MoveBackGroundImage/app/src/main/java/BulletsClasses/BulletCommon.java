package BulletsClasses;

import android.graphics.Bitmap;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.TargetBird;
import com.google.android.gms.example.movebackgroundimage.TargetBirdOne;

import CustomDialogBox.DialogBoxCustom;

/**
 * Created by sadiq on 9/7/2016.
 */
public class BulletCommon {

    public boolean resetBullet = true; //for bullet Reset

    public GamePanel gamePanelView;
    public Bitmap bulletBitMap;
    public int x = 0;
    public int y = 0;
    public TargetBirdOne targetBirdOne;
    public TargetBird targetBird;

    public BulletCommon(GamePanel gamePanel, Bitmap bulletOneBitMap, int x, int y, TargetBird targetBird, TargetBirdOne targetBirdOne){
        this.gamePanelView = gamePanel;
        this.bulletBitMap = bulletOneBitMap;
        this.x = x;
        this.y = y;
        this.targetBirdOne = targetBirdOne;
        this.targetBird = targetBird;
    }

    /**
     * Update the state of the sprite.
     */
    public void updateBullet(){
        if(GamePanel.isFired){

            if ( this.x < this.gamePanelView.getBackgroundTotalWidth() && GamePanel.isFired){
                this.x += 20;

                if ( this.x == 1000){
                } else if (this.x == 1020){
                }
                //isCollide();

                if ( this.x == ( this.gamePanelView.getBackgroundTotalWidth() - 4) ){
                    this.x = this.gamePanelView.getBackgroundTotalWidth();

                }

                if (this.x == this.gamePanelView.getBackgroundTotalWidth() ){ //&& GamePanel.bulletCount == 0
                    /*this.resetBullet = false; //for bullet Reset
                    GamePanel.isFired = false;//=setBulletFired(false);*/

                    switch(GamePanel.bulletCount ){
                        case 0:

                            //GamePanel.bulletTestOne.setBulletOneX(this.x);
                            //GamePanel.bulletTestOne = null;
                            break;

                        case 1:

                            //GamePanel.bulletTestTwo.setBulletTwoX(this.x);
                            GamePanel.bulletCount = -1;
                            //GamePanel.bulletTestTwo = null;
                            break;

                        case 2:
                            /*Log.e("abc", "here GamePanel.bulletCount 2 => "+ GamePanel.bulletCount);
                            GamePanel.bulletTestThree.setBulletThreeX(this.x);
                            GamePanel.bulletTestThree = null;
                            GamePanel.bulletCount = -1;*/
                            break;
                    }

                    /*Log.e("BulletCommon","this.gamePanelView.getBackgroundTotalWidth() if => " + this.gamePanelView.getBackgroundTotalWidth() );
                    Log.e("BulletCommon","this.x if => " + this.x );
                    Log.e("BulletCommon","GamePanel.isFired if => " + GamePanel.isFired );
                    Log.e("BulletCommon","this.resetBullet if => " + this.resetBullet +"\n============");*/

                    this.resetBullet = false; //for bullet Reset
                    GamePanel.isFired = false;//=setBulletFired(false);

                }
            }
            //else if (this.x == this.gamePanelView.getBackgroundTotalWidth()

            else if (this.x == this.gamePanelView.getBackgroundTotalWidth()
                    && !GamePanel.isFired
                    && !this.resetBullet
                    ) {
                /*Log.e("BulletCommon","this.gamePanelView.getBackgroundTotalWidth() else if => " + this.gamePanelView.getBackgroundTotalWidth() );
                Log.e("BulletCommon","this.x else if => " + this.x );
                Log.e("BulletCommon","GamePanel.isFired else if => " + GamePanel.isFired );
                Log.e("BulletCommon","this.resetBullet else if  => " + this.resetBullet +"\n============");*/

                //GamePanel.bulletTestOne.setBulletOneX(0);
                //GamePanel.bulletTestTwo.setBulletTwoX(0);
                //GamePanel.bulletTestThree.setBulletThreeX(0);
                //this.x = 0;
                GamePanel.isFired = false;//=setBulletFired(false);
            }
        }
    }


    /* working code
    public void updateBullet(){
        if(GamePanel.isFired){

            Log.e("abc", "GamePanel.bulletCount => "+ GamePanel.bulletCount);
            if ( this.x < this.gamePanelView.getBackgroundTotalWidth() && GamePanel.isFired){
                this.x += 20;
                isCollide();
                if ( this.x == ( this.gamePanelView.getBackgroundTotalWidth() - 4) ){
                    this.x = this.gamePanelView.getBackgroundTotalWidth();
                }

                if (this.x == this.gamePanelView.getBackgroundTotalWidth() ){//&& GamePanel.bulletCount == 0
                    this.resetBullet = false; //for bullet Reset
                    GamePanel.isFired = false;//=setBulletFired(false);

                    GamePanel.bulletCount = 0;
                    //GamePanel.bulletTestOne = null;
                }
            }
        } else if (this.x == this.gamePanelView.getBackgroundTotalWidth() && !GamePanel.isFired && !this.resetBullet ) {
            this.x = 0;
            GamePanel.isFired = false;//=setBulletFired(false);
        }
    }*/

    public void isCollide(){
        if ( this.x  >= this.targetBird.getX()
                && this.y >= (this.targetBird.getY() )
                && this.y <= (this.targetBird.getY() + this.targetBird.getHeight())
                ){
            isCollision();

        } else if ( this.x  >= this.targetBirdOne.getX()
                && this.y >= (this.targetBirdOne.getY() )
                && this.y <= (this.targetBirdOne.getY() + this.targetBirdOne.getHeight())
                ){
            isCollisionOne();

        }
        else if (this.x > (this.gamePanelView.getBackgroundTotalWidth()-10) && GamePanel.totalChance > 0 ){//&& !isCollide
            GamePanel.totalChance += -1;

        }

        if (GamePanel.totalChance == 0){ //for showing dialog box
            DialogBoxCustom.isGameOver = true;
        }
    }

    private void isCollisionOne(){
        if ( this.x  >= this.targetBirdOne.getX()
                && this.y >= (this.targetBirdOne.getY() )
                && this.y <= (this.targetBirdOne.getY() + this.targetBirdOne.getHeight())
                ){

            this.x = this.gamePanelView.getBackgroundTotalWidth();//1024;
            GamePanel.scoreValue += 5;
            this.targetBirdOne.setY(520);
            this.targetBirdOne.targetBirdBoolean(true);

        }else if (this.x > (this.gamePanelView.getBackgroundTotalWidth()-10) && GamePanel.totalChance > 0 ){//&& !isCollide
            GamePanel.totalChance += -1;
        }

        if (GamePanel.totalChance == 0){ //for showing dialog box
            DialogBoxCustom.isGameOver = true;
        }
    }

    private void isCollision(){
        if ( this.x  >= this.targetBird.getX()
                && this.y >= (this.targetBird.getY() )
                && this.y <= (this.targetBird.getY() + this.targetBird.getHeight())
                ){

            this.x = this.gamePanelView.getBackgroundTotalWidth();//1024;
            GamePanel.scoreValue += 5;
            this.targetBird.setY(0);
            this.targetBird.targetBirdBoolean(true);

        }else if (this.x > (this.gamePanelView.getBackgroundTotalWidth()-10) && GamePanel.totalChance > 0 ){//&& !isCollide
            GamePanel.totalChance += -1;
        }

        if (GamePanel.totalChance == 0){ //for showing dialog box
            DialogBoxCustom.isGameOver = true;
        }
    }
}
