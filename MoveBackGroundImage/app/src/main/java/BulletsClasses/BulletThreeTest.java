package BulletsClasses;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.TargetBird;
import com.google.android.gms.example.movebackgroundimage.TargetBirdOne;

/**
 * Created by sadiq on 9/8/2016.
 */
public class BulletThreeTest extends BulletCommon {
    private int bulletThreeX = 0;

    public BulletThreeTest(GamePanel gamePanel, Bitmap bulletEightBitMap, int x, int y, TargetBird targetBird, TargetBirdOne targetBirdOne){
        super(gamePanel,bulletEightBitMap , x , y , targetBird , targetBirdOne);


    }

    public void onDraw(Canvas canvas){
        updateBullet();
        canvas.drawBitmap(bulletBitMap, x, y, null);
    }

    /**
     * bullet X
     */
    public void setBulletThreeX(int x){
        this.bulletThreeX = x;
    }
}
