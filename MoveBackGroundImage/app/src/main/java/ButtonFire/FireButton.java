package ButtonFire;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;

/**
 * Created by sadiq on 9/22/2016.
 */
public class FireButton {

    private GamePanel gamePanelFireButton;
    private Bitmap bitmapFireButton;
    private int x, y, fireButtonHeight, fireButtonWidth;

    public FireButton(GamePanel gamePanelFireButton, Bitmap bitmapFireButton, int x, int y ){
        this.gamePanelFireButton = gamePanelFireButton;
        this.bitmapFireButton = bitmapFireButton;
        this.x = x;
        this.y = y;
        this.fireButtonHeight = bitmapFireButton.getHeight();
        this.fireButtonWidth = bitmapFireButton.getWidth();
    }

    public void draw(Canvas canvas){
        canvas.drawBitmap(this.bitmapFireButton, this.x, this.y, null);
    }

    public int getGireButtonHeight(){
        return this.fireButtonHeight;
    }

    public int getGireButtonWidth(){
        return this.fireButtonWidth;
    }

}
