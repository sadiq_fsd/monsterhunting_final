package LifePack;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.MainThread;

/**
 * Created by sadiq on 10/5/2016.
 */
public class LifePacks {
    private GamePanel gamePanel;
    private Bitmap bmpScore;
    private int x =0;
    private int y =0;
    private int width, height;
    private int xSpeed =0;
    public static boolean lifePackBoolean = false;
    public static int RANDOM_LIFE_VALUE;

    private int rYValueLifePack = 0;
    private int rXValueLifePack ;

    public LifePacks( GamePanel gamePanel, Bitmap bmpScore, int x, int y ){
        this.gamePanel = gamePanel;
        this.bmpScore = bmpScore;
        this.x = x;
        this.y = y;
        this.height = bmpScore.getHeight();
        this.width = bmpScore.getWidth();

        this.rXValueLifePack = this.x;
        this.rYValueLifePack = this.y;
    }

    public void draw(Canvas canvas){
        if (MainThread.healthTimerCounter >= 450){ //450
            update();
            RANDOM_LIFE_VALUE = gamePanel.getRandomBulletsChance();
        }
        canvas.drawBitmap(this.bmpScore, this.rXValueLifePack, this.rYValueLifePack, null);
    }

    private void update() {
        if (x  > 0) {
            xSpeed = -6;
        }

        //if (x + width  < 0) { //working
        if (this.rXValueLifePack + width  < 0) { //working
            //x = GamePanel.WIDTH;
            this.rXValueLifePack = this.gamePanel.getRandomValueX();
            this.rYValueLifePack = this.gamePanel.getRandomValue();
            xSpeed = -6;
            MainThread.healthTimerCounter =0;
        }
        //x = x + xSpeed;
        this.rXValueLifePack = this.rXValueLifePack + xSpeed;
    }

    public int getLifePackX(){
        //return this.x;
        return this.rXValueLifePack;
    }

    public void setLifePackX(int x){
        //this.x = x;
        this.rXValueLifePack = x;
    }

    public int getLifePackY(){
        //return this.y;
        return this.rYValueLifePack;
    }

    public void setLifePackY(int y){
        //this.y = y;
        this.rYValueLifePack = y;
    }

    public int getHeightLifePack(){
        return this.height;
    }

    public int getWidthLifePack(){
        return this.width;
    }
}
