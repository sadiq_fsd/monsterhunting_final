package DatabaseFolder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import ObjectsClasses.ScoreRecord;

/**
 * Created by sadiq on 11/9/2016.
 */
public class Dbhandler extends SQLiteOpenHelper{

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "scoreRecordsInfo";
    // Contacts table name
    private static final String TABLE_SCORE = "score";

    // Shops Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "user_name";
    private static final String KEY_USER_SCORE = "user_score";

    int count = 0;

    public Dbhandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLES = "CREATE TABLE "+ TABLE_SCORE + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_NAME + " TEXT, "
                + KEY_USER_SCORE + " INTEGER" + ")";
        db.execSQL(CREATE_CONTACTS_TABLES);
    }

    /**==============================================================================**/

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORE );
        onCreate(db);
    }

    /**==============================================================================**/

    // Adding new ScoreRecord
    public void addScoreRecord(ScoreRecord scoreRecord) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, scoreRecord.get_name()); // Shop Name
        values.put(KEY_USER_SCORE, scoreRecord.get_score()); // Shop Phone Number
        // Inserting Row
        db.insert(TABLE_SCORE, null, values);
        db.close(); // Closing database connection
    }

    /**==============================================================================**/

    // Getting single ScoreRecord
    public ScoreRecord getScoreRecord(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SCORE, new String[] { KEY_ID,
                        KEY_NAME, KEY_USER_SCORE }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        ScoreRecord contact = new ScoreRecord(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), Integer.parseInt(cursor.getString(2)));
        // return scoreRecord
        return contact;
    }

    /**==============================================================================**/

    // Getting Max ScoreRecord
    public ScoreRecord getMaxScoreRecord() {
        SQLiteDatabase db = this.getReadableDatabase();
        String maxScoreQuery = "SELECT * FROM "+TABLE_SCORE+" ORDER BY "+KEY_USER_SCORE+" DESC";

        Cursor cursor = null;
        try {
            cursor = db.rawQuery(maxScoreQuery, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Cursor cursor = db.query(TABLE_SCORE, new String[]{"MAX(" + KEY_USER_SCORE + ")"}, null, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        ScoreRecord contact = new ScoreRecord(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), Integer.parseInt(cursor.getString(2)));
        // return scoreRecord
        return contact;
    }

    /**==============================================================================**/

    // Getting All ScoreRecord
    public ArrayList<ScoreRecord> getAllScoreRecord() {
        ArrayList<ScoreRecord> scoreRecordList = new ArrayList<ScoreRecord>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_SCORE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ScoreRecord scoreRecord = new ScoreRecord();
                scoreRecord.set_id(Integer.parseInt(cursor.getString(0)));
                scoreRecord.set_name(cursor.getString(1));
                scoreRecord.set_score(Integer.parseInt(cursor.getString(2)));
                // Adding contact to list
                scoreRecordList.add(scoreRecord);
            } while (cursor.moveToNext());
        }
        // return contact list
        return scoreRecordList;
    }

    /**==============================================================================**/

    // Getting ScoreRecord Count
    public int getScoreRecordCount() {
        String countQuery = "SELECT * FROM " + TABLE_SCORE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        count = cursor.getCount();
        cursor.close();
        return count;
    }

    /**==============================================================================**/

    // Updating a ScoreRecord
    public int updateScoreRecord(ScoreRecord scoreRecord) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, scoreRecord.get_name());
        values.put(KEY_USER_SCORE, scoreRecord.get_score());
        // updating row
        return db.update(TABLE_SCORE, values, KEY_ID + " = ?",
                new String[]{String.valueOf(scoreRecord.get_id())});
    }

    /**==============================================================================**/

    // Deleting a ScoreRecord
    public void deleteScoreRecord(ScoreRecord scoreRecord) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SCORE, KEY_ID + " = ?",
                new String[]{String.valueOf(scoreRecord.get_id())});
        db.close();
    }

    /**==============================================================================**/
}
