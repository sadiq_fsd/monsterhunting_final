package DirectionPad;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.Gun;

/**
 * Created by sadiq on 10/7/2016.
 */
public class DirectionLeft {

    private GamePanel gamePanel;
    private Bitmap bitmapMoveDown;
    private Gun diver;
    private int x, y,  bitMapHeight, bitMapWidth;


    public DirectionLeft(GamePanel gamePanel, Bitmap bitmap, int x, int y, Gun diver){
        this.gamePanel = gamePanel;
        this.bitmapMoveDown = bitmap;
        this.x = x;
        this.y = y;
        this.bitMapHeight = bitmap.getHeight();
        this.bitMapWidth = bitmap.getWidth();
        this.diver = diver;
    }

    public void draw(Canvas canvas){
        canvas.drawBitmap(this.bitmapMoveDown, this.x, this.y, null);
    }

    public int getMoveDownHeight(){
        return this.bitMapHeight;
    }

    public int getMoveDownWidth(){
        return this.bitMapWidth;
    }
}
