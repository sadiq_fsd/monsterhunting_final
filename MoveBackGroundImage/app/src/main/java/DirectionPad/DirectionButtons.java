package DirectionPad;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.Gun;

/**
 * Created by sadiq on 10/6/2016.
 */
public class DirectionButtons {

    private GamePanel gamePanel;
    private Bitmap bmpScore;
    private Gun gun;
    private int x =0;
    private int y =0;
    private int gunY, gunX;
    private int imageWidth, imageHeight;
    //public static int totalHeight = 3;//can not be equal to zero
    private int totalHeight = 3;//can not be equal to zero
    public static int totalWidth = 10;//3;//can not be equal to zero
    boolean isUpMove = false;
    boolean isUpMoveLeftRight = false;
    boolean isUpMoveGameOver = false;


    public DirectionButtons( GamePanel gamePanel, Bitmap bmpScore, int x, int y, Gun gun ){
        this.gamePanel = gamePanel;
        this.bmpScore = bmpScore;
        this.x = x;
        this.y = y;
        this.imageHeight = bmpScore.getHeight();
        this.imageWidth = bmpScore.getWidth();
        this.gun = gun;
    }

    public void draw(Canvas canvas){
        moveYupDirection();
        moveYdownDirection();
        moveXforwardDirection();
        moveXbackwardDirection();
        canvas.drawBitmap(this.bmpScore, this.x, this.y, null);
    }

    //moves right direction
    public void moveXforwardDirection(){
        if (GamePanel.isMoveRight){
            if ( DirectionButtons.totalWidth < (GamePanel.WIDTH - this.gun.getDiverWidthSpriteSheet()) && this.isUpMoveLeftRight ){//&& this.isUpMoveLeftRight
                DirectionButtons.totalWidth = this.gun.getGunX() + this.gun.getDiverWidthSpriteSheet();
                gunX = this.gun.getGunX();
                //gunX = gunX + 3;
                gunX = gunX + (int)(GamePanel.WIDTH/199.333);//6;
                this.gun.setGunX(gunX);
                setMoveUpX(gunX);
            } else {
                GamePanel.isMoveRight = false;
                setMoveLeftRightBoolean(false);
            }
        }
    }

    //moves left direction
    public void moveXbackwardDirection(){
        if ( GamePanel.isMoveLeft ){
            if ( DirectionButtons.totalWidth < 10 ){
                DirectionButtons.totalWidth = 10;
                GamePanel.isMoveLeft = false;
                setMoveLeftRightBoolean(false);
            } else if ( DirectionButtons.totalWidth <= (GamePanel.WIDTH - this.gun.getDiverWidthSpriteSheet()) && this.isUpMoveLeftRight){
                DirectionButtons.totalWidth = this.gun.getGunX() + this.gun.getDiverWidthSpriteSheet();
                gunX = this.gun.getGunX();
                //gunX = gunX - 3;
                gunX = gunX - (int)(GamePanel.WIDTH/199.333);//6;
                this.gun.setGunX(gunX);
                setMoveUpX(gunX);
            } else if( DirectionButtons.totalWidth > (GamePanel.WIDTH - this.gun.getDiverWidthSpriteSheet()) && this.isUpMoveLeftRight ){
                DirectionButtons.totalWidth = GamePanel.WIDTH - this.gun.getDiverWidthSpriteSheet();
            } /*else {
                GamePanel.isMoveLeft = false;
                setMoveLeftRightBoolean(false);
            }*/
        }
    }

    //moves up direction
    public void  moveYupDirection(){
        if (GamePanel.isMoveUpDown){
            //if ( MoveUp.totalHeight <  this.gamePanel.getBackgroundTotalHeight() && this.isUpMove ){
            if ( totalHeight <  GamePanel.HEIGHT && this.isUpMove ){
                if ( totalHeight <= 0){
                    GamePanel.isMoveUpDown = false;
                    setMoveUpBoolean(false);
                }else{
                    totalHeight = this.gun.getGunY();
                    gunY = this.gun.getGunY();
                    //gunY = gunY - 3;
                    gunY = gunY - (int)(GamePanel.WIDTH/199.333);//6;
                    this.gun.setGunY(gunY);
                    setMoveUpY(gunY);
                }
            }
        }
    }

    //moves down direction

    public void  moveYdownDirection(){
        if (GamePanel.isMoveUpDown){
            if ( totalHeight <  GamePanel.HEIGHT && !isUpMove){
                if ( totalHeight >= (GamePanel.HEIGHT - GamePanel.fireButtonHeight - gun.getDiverHeightSpriteSheet())){ // move down
                    GamePanel.isMoveUpDown = false;
                    setMoveUpBoolean(true);
                }else  {
                    totalHeight = this.gun.getGunY();
                    gunY = this.gun.getGunY();
                    //gunY = gunY + 3;
                    gunY = gunY + (int)(GamePanel.WIDTH/199.333);//6;
                    this.gun.setGunY(gunY);
                    setMoveUpY(gunY);
                }
            }
        }
    }

    private void setMoveUpY(int y){
        totalHeight = y;
    }

    /*public int getMoveUpYTotalHeightAfterGameOver(){
        return totalHeight ;
    }*/

    private void setMoveUpX(int x){
        DirectionButtons.totalWidth = x;
    }

    public void setMoveUpBoolean(boolean b){
        this.isUpMove = b;
    }

    public void setMoveLeftRightBoolean(boolean b){
        this.isUpMoveLeftRight = b;
    }

    public void  moveYdownDirectionGameOver(){

        if (GamePanel.isMoveUpDownGameOver){
            GamePanel.isMoveRight = false;
            GamePanel.isMoveLeft = false;

            if ( totalHeight <  GamePanel.HEIGHT && !isUpMoveGameOver){
                if ( totalHeight >= (GamePanel.HEIGHT - GamePanel.fireButtonHeight - gun.getDiverHeightSpriteSheet())){ // move down
                    GamePanel.isMoveUpDownGameOver = false;
                    setMoveUpBoolean(true);
                }else  {
                    totalHeight = this.gun.getGunY();
                    gunY = this.gun.getGunY();
                    //gunY = gunY + 3;
                    gunY = gunY + (int)(GamePanel.WIDTH/199.333);//6;
                    this.gun.setGunY(gunY);
                    setMoveUpY(gunY);
                }
            }
        }
    }
}
