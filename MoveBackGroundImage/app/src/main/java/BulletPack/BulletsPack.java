package BulletPack;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.Gun;
import com.google.android.gms.example.movebackgroundimage.MainThread;

/**
 * Created by sadiq on 10/5/2016.
 */
public class BulletsPack {

    private GamePanel gamePanel;
    private Bitmap res;
    private int x =0;
    private  int y = 0;
    private int width ;
    private  int xSpeed;
    private int height ;
    private Gun diver;
    public static int RANDOM_BULLETS_VALUE; // randomBulletsValue;

    //public static int rYValueBulletPack = 0;
    //public static int rXValueBulletPack ;

    private int rYValueBulletPack = 0;
    private int rXValueBulletPack ;

    public BulletsPack(GamePanel gamePanel, Bitmap res, int x, int y){
        this.gamePanel = gamePanel;
        this.res = res;
        this.x = x;
        this.y = y;
        this.width = res.getWidth();
        this.height = res.getHeight();
        //this.diver = diver;

        this.rXValueBulletPack = this.x;
        this.rYValueBulletPack = this.y;
    }

    public void draw(Canvas canvas){
        //Log.e("MainThreadTimerCounter", "MainThreadTimerCounter=> " + MainThread.timerCounter);
        if (MainThread.bulletTimerCounter >= 800){ //450
            update();
            RANDOM_BULLETS_VALUE = gamePanel.getRandomBulletsChance();
        }
        //canvas.drawBitmap(this.res, this.x, this.y, null);
        canvas.drawBitmap(this.res, this.rXValueBulletPack, this.rYValueBulletPack, null);
    }

    private void update() {
        //if (x  > 0) {
        if (this.rXValueBulletPack + width > 0) {
            xSpeed = -8;
        }

        //if (x + width  < 0) { //working
        if (this.rXValueBulletPack + width  < 0) {
            //x = GamePanel.WIDTH;
            this.rXValueBulletPack = this.gamePanel.getRandomValueX();
            this.rYValueBulletPack = this.gamePanel.getRandomValue();
            xSpeed = -8;
            MainThread.bulletTimerCounter =0;
        }
        //x = x + xSpeed;

        this.rXValueBulletPack = this.rXValueBulletPack + xSpeed;
    }

    public int getBulletPackX(){
        //return this.x;
        return this.rXValueBulletPack;
    }

    public void setBulletPackX(int x){
        //this.x = x;
        this.rXValueBulletPack = x;
    }

    public int getBulletPackY(){
         //return this.y;
         return this.rYValueBulletPack;
    }

    public void setBulletPackY(int y){
        //this.y = y;
        this.rYValueBulletPack = y;
    }

    public int getHeightBulletPack(){
        return this.height;
    }

    public int getWidthBulletPack(){
        return this.width;
    }

}
