package ObjectsClasses;

/**
 * Created by sadiq on 11/9/2016.
 */
public class ScoreRecord {
    int _id, _score;
    String _name;


    public ScoreRecord(){}

    public ScoreRecord(int id, String name, int score){
        set_id(id);
        set_name(name);
        set_score(score);
    }


    public ScoreRecord(String name, int score){
        set_score(score);
        set_name(name);
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public void set_score(int _score) {
        this._score = _score;
    }

    public int get_id() {
        return _id;
    }

    public int get_score() {
        return _score;
    }

    public String get_name() {
        return _name;
    }

}
