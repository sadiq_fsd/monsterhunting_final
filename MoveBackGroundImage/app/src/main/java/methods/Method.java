package methods;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by sadiq on 11/10/2016.
 */
public class Method {

    public static String getDeviceIdProgram(Context context){
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }


}
