package ScoreFolder;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;

/**
 * Created by sadiq on 10/4/2016.
 */
public class ScoreImage {

    private GamePanel gamePanel;
    private Bitmap bmpScore;
    private int x =0;
    private int y =0;
    private int imageWidth, imageHeight;

    public ScoreImage( GamePanel gamePanel, Bitmap bmpScore, int x, int y ){
        this.gamePanel = gamePanel;
        this.bmpScore = bmpScore;
        this.x = x;
        this.y = y;
        this.imageHeight = bmpScore.getHeight();
        this.imageWidth = bmpScore.getWidth();
    }

    public void draw(Canvas canvas){

        canvas.drawBitmap(this.bmpScore, this.x, this.y, null);
    }

}
