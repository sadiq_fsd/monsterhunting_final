package CustomDialogBox;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.example.movebackgroundimage.ClickToPaly;
import com.google.android.gms.example.movebackgroundimage.Game;
import com.google.android.gms.example.movebackgroundimage.R;

/**
 * Created by sadiq on 11/9/2016.
 */
public class DialogBoxTakeName implements View.OnClickListener{
//public class DialogBoxTakeName extends Dialog implements View.OnClickListener{

    Context _context;
    String _play;
    Dialog _dialog;
    EditText _user_name;

    public DialogBoxTakeName(Context c, String play){
        _context = c;
        _play = play;

        callSwitchMethod();
    }

    private void callSwitchMethod(){
        switch (_play){
            case "play":
                takeNameDialogBox();
                break;
            case "gameOver":
                break;
        }
    }

    private void takeNameDialogBox(){
        _dialog = new Dialog(_context);
        _dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        _dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        _dialog.setContentView(R.layout.take_name_from_user);

        _user_name = (EditText) _dialog.findViewById(R.id.user_name);

        Button user_ok = (Button) _dialog.findViewById(R.id.user_ok);
        user_ok.setOnClickListener(this);

        _dialog.show();

    }

    @Override
    public void onClick(View v) {
        int clickId = v.getId();
        switch (clickId){
            case R.id.user_ok:

                if ( _user_name.getText().toString().length() >= 3 ){
                    ClickToPaly._userNameBeforeGameStart = _user_name.getText().toString();
                } else if ( _user_name.getText().toString().length() < 3 || _user_name.getText().toString().equals("") ) {
                    _user_name.setError("Enter Your Username atleast 3 characters!");
                    return;
                }

                //backGroundSoundBoolean true => play
                //backGroundSoundBoolean false => stop

                ClickToPaly.backGroundSound.stop();
                ClickToPaly.backGroundSound = new MediaPlayer();
                ClickToPaly.backGroundSound = ClickToPaly.backGroundSound.create(_context, R.raw.background);
                ClickToPaly.backGroundSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        ClickToPaly.backGroundSound.start();
                    }
                });
                ClickToPaly.backGroundSound.start();




                Intent i = new Intent(_context, Game.class);
                _context.startActivity(i);
                break;
        }
    }



}
