package CustomDialogBox;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.example.movebackgroundimage.ClickToPaly;
import com.google.android.gms.example.movebackgroundimage.R;

/**
 * Created by sadiq on 9/21/2016.
 */
public class CloseApplicationDialog implements View.OnClickListener {
    //call from click to play activity
    private Context context;
    //private Dialog dialog;//, dialogNew;
    private Dialog dialogNew;

    public CloseApplicationDialog(Context context){
        this.context = context;
        //dialogBox();
        dialogBoxNew();
    }

    private void dialogBoxNew(){
        dialogNew = new Dialog(this.context);
        dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNew.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogNew.setContentView(R.layout.custom_new);



        //dialogNew.setTitle("Monster Hunting");

        // set the custom dialog components - text, image and button_close
        /*TextView text = (TextView) dialogNew.findViewById(R.id.text);
        text.setText("Are you sure to Exit this application");*/

        //ImageView backgroundImage = (ImageView) dialogNew.findViewById(R.id.image_bg);
        //backgroundImage.setImageResource(R.drawable.button_background);

        TextView dialogButton = (TextView) dialogNew.findViewById(R.id.play_button_image);
        ImageView dialogButtonNo = (ImageView) dialogNew.findViewById(R.id.cancel_action);
        TextView are_you_sure_text = (TextView) dialogNew.findViewById(R.id.are_you_sure_text);
        TextView to_exit_text = (TextView) dialogNew.findViewById(R.id.to_exit_text );


        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/porkys.ttf");
        Typeface bold = Typeface.create(typeface, Typeface.BOLD);

        are_you_sure_text.setText(R.string.are_you_sure_text);
        //are_you_sure_text.setTextSize(35);
        are_you_sure_text.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.toExitText));
        are_you_sure_text.setTextColor(Color.parseColor("#800000"));
        are_you_sure_text.setTypeface(bold);

        to_exit_text.setText(R.string.to_exit_text);
        to_exit_text.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.toExitText));
        //to_exit_text.setTextSize(R.dimen.toExitText);
        to_exit_text.setTextColor(Color.parseColor("#800000"));
        to_exit_text.setTypeface(bold);

        dialogButton.setText(R.string.dialog_button_close);
        //dialogButton.setTextSize(30);
        dialogButton.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.closeButton));
        dialogButton.setTextColor(Color.parseColor("#FFFFFF"));
        dialogButton.setTypeface(bold);


        dialogButton.setOnClickListener(this);
        dialogButtonNo.setOnClickListener(this);

        dialogNew.show();
    }

    private void dialogBox() {

        // custom dialog
        /*dialog = new Dialog(this.context);
        dialog.setContentView(R.layout.custom);
        dialog.setTitle("Monster Hunting");

        // set the custom dialog components - text, image and button_close
        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText("Are you sure to Exit this application");

        ImageView image = (ImageView) dialog.findViewById(R.id.image);
        image.setImageResource(R.drawable.close_btn);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        Button dialogButtonNo = (Button) dialog.findViewById(R.id.dialogButtonNo);
        // if button_close is clicked, close the custom dialog
        *//*dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*//*

        dialogButton.setOnClickListener(this);
        dialogButtonNo.setOnClickListener(this);

        dialog.show();*/
    }

    @Override
    public void onClick(View v) {
        int eleId = v.getId();

        switch(eleId){
            case R.id.play_button_image:
                ClickToPaly.backGroundSound.stop();
                ((Activity)context).finish();
                break;
            case R.id.cancel_action:
                //dialog.dismiss();
                dialogNew.dismiss();
                break;
        }
    }
}
