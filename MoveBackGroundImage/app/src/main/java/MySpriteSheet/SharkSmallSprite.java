package MySpriteSheet;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.google.android.gms.example.movebackgroundimage.GamePanel;

/**
 * Created by sadiq on 10/7/2016.
 */
public class SharkSmallSprite {

    private static final int BMP_ROWS = 3;
    private static final int BMP_COLUMNS = 3;
    private int x = 0;
    private int y = 0;
    private int xSpeed = -(GamePanel.WIDTH/299);//4

    private GamePanel gamePanel;
    private Bitmap bitmap;
    private int currentFrame = 0;
    private int currentFrameY = 0;
    private int width;
    private int height;

    private int abc = 0;
    private int srcY = 0;
    private int srcX =0;

    public static int rYValue = 0;
    public static int rXValue ;

    //public static int testY = 0;

    public SharkSmallSprite(GamePanel gamePanel, Bitmap bitmap, int x, int y){
        this.gamePanel = gamePanel;
        this.bitmap = bitmap;
        this.width = bitmap.getWidth() / BMP_COLUMNS;
        this.height = bitmap.getHeight() / BMP_ROWS;
        this.x = x;
        this.y = y;
        this.rXValue = this.x;
        //this.testY = y;
    }

    private void update() {
        if (this.rXValue + width > 0) {
            xSpeed = -(GamePanel.WIDTH/299);//4
        }

        if (this.rXValue + width  < 0) {
            this.rYValue = this.gamePanel.getRandomValue();
            this.rXValue = this.gamePanel.getRandomValueX();
            this.rXValue = this.rXValue + 100;
            xSpeed = -(GamePanel.WIDTH/299);//4;
        }

        this.rXValue = this.rXValue + xSpeed;

        if( currentFrame ==2 || currentFrame ==3)
            currentFrameY++;
        if(currentFrameY ==2 || currentFrameY ==3)
            currentFrameY = currentFrameY % BMP_COLUMNS;

        currentFrame = ++currentFrame % BMP_COLUMNS;
    }

    public void draw(Canvas canvas){
        update();
        if (abc == 0){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 1 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 2 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 3 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 4 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 5 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 6 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 7 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 8 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        }

        Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);

        Rect dst;
        if ( rYValue == 0){
            dst = new Rect(this.rXValue, y, this.rXValue + width, y + height);
        } else {
            dst = new Rect(this.rXValue, this.rYValue, this.rXValue + width, this.rYValue + height);
        }

        canvas.drawBitmap(bitmap, src, dst, null);
        //canvas.drawBitmap(bitmap, this.rXValue, testY, null);

        if (abc == 9){
            abc = 0;
        }
    }

    public int getHeightSpriteSheet(){
        return this.height;
    }

    public int getWidthSpriteSheet(){
        return this.width;
    }

    public int getSpriteSheetX(){
        return this.rXValue;
    }

    public int getSpriteSheetY(){
        return this.y;
    }

    public int getSpriteSheeRandomY(){
        return this.rYValue;
    }

    public void setSpriteSheetX(int x){
        this.rXValue = x;
    }

    public void setSpriteSheetY(int y){
        this.y = y;
    }
}
