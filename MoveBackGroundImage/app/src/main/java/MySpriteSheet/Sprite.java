package MySpriteSheet;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.google.android.gms.example.movebackgroundimage.GamePanel;

/**
 * Created by sadiq on 9/9/2016.
 */
public class Sprite {
    private static final int BMP_ROWS = 3;
    private static final int BMP_COLUMNS = 3;
    private int x = 0;
    private int y = 0;
    private int xSpeed = -(int)(GamePanel.WIDTH/119.6);//10;
    //private int xSpeed = -4;

    private GamePanel gamePanel;
    private Bitmap bitmap;
    private int currentFrame = 0;
    private int currentFrameY = 0;
    private int width;
    private int height;

    private int abc = 0;
    private int srcY = 0;
    private int srcX =0;

    //private int rValue = 0;
    public static int rYValue = 0;
    public static int rXValue ;

    public Sprite(GamePanel gamePanel, Bitmap bitmap, int x, int y){
        this.gamePanel = gamePanel;
        this.bitmap = bitmap;
        this.width = bitmap.getWidth() / BMP_COLUMNS;
        this.height = bitmap.getHeight() / BMP_ROWS;
        this.x = x;
        this.y = y;
        this.rXValue = this.x;
    }

    private void update() {
        //if (x  > 0) {
        //if (x > gamePanel.getWidth() - width - xSpeed) { //working

        if (this.rXValue + width > 0) {
            //xSpeed = -4;
            xSpeed = -(int)(GamePanel.WIDTH/119.6);//10;;
        }

        //if (x + width  < 0) { //working

        if (this.rXValue + width  < 0) {
            this.rYValue = this.gamePanel.getRandomValue();
            this.rXValue = this.gamePanel.getRandomValueX();
            //x = gamePanel.getWidth();
            //xSpeed = -4;
            xSpeed = -(int)(GamePanel.WIDTH/119.6);//10;
        }
        //x = x + xSpeed;
        this.rXValue = this.rXValue + xSpeed;

        if( currentFrame ==2 || currentFrame ==3)
            currentFrameY++;
        if(currentFrameY ==2 || currentFrameY ==3)
            currentFrameY = currentFrameY % BMP_COLUMNS;

        currentFrame = ++currentFrame % BMP_COLUMNS;
    }

    public void draw(Canvas canvas){
        update();
        if (abc == 0){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 1 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 2 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 3 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 4 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 5 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 6 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 7 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        } else if (  abc == 8 ){
            srcX = currentFrame * width;
            srcY = currentFrameY * height;
            abc = abc + 1;
        }

        Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
        //Rect dst = new Rect(x, y, x + width, y + height);
        //Rect dst = new Rect(x, this.rYValue, x + width, this.rYValue + height);

        Rect dst;
        if ( rYValue == 0){
            dst = new Rect(rXValue, y, rXValue + width, y + height);
        } else {
            dst = new Rect(rXValue, rYValue, rXValue + width, rYValue + height);
        }

        canvas.drawBitmap(bitmap, src, dst, null);

        if (abc == 9){
            abc = 0;
        }
    }

    public int getHeightSpriteSheet(){
        return this.height;
    }

    public int getWidthSpriteSheet(){
        return this.width;
    }

    public int getSpriteSheetX(){
        //return this.x;
        return this.rXValue;
    }

    public int getSpriteSheetY(){
        return this.y;
    }

    public int getSpriteSheeRandomY(){
        return this.rYValue;
    }

    public void setSpriteSheetX(int x){
        //this.x = x;
        this.rXValue = x;
    }

    public void setSpriteSheetY(int y){
        this.y = y;
    }

    public int getSpriteLeavingTopSpaceForFin(){
        float whaleTopSpace = 0.0f;
        int whaleSpaceInt = 0;
        whaleTopSpace = (float) (getSpriteSheeRandomY() + (this.height/3.03));//currentY + 60
        //whaleTopSpace = (float) (getSpriteSheeRandomY() + (this.height/4.55));//currentY + 40
        whaleSpaceInt = (int) Math.ceil( whaleTopSpace );
        return whaleSpaceInt;
    }

    public int getSpriteLeavingBottomSpace(){//used this one
        float whaleBottomSpace = 0.0f;
        int whaleSpaceIntBottom = 0;
        whaleBottomSpace = (float) (getSpriteSheeRandomY() + getHeightSpriteSheet() - (getHeightSpriteSheet()/9.1));//currentY+ getHeightSpriteSheet() + 20
        whaleSpaceIntBottom = (int) Math.ceil( whaleBottomSpace );
        return whaleSpaceIntBottom;
    }

    public int getSpriteLeavingBottomSpaceNew(){ //not in use
        float whaleBottomSpaceNew = 0.0f;
        int whaleSpaceIntBottomNew = 0;
        whaleBottomSpaceNew = (float) (getSpriteSheeRandomY() + getHeightSpriteSheet() - (getHeightSpriteSheet()/3.64));//currentY+ getHeightSpriteSheet() -50
        whaleSpaceIntBottomNew = (int) Math.ceil( whaleBottomSpaceNew );

        return whaleSpaceIntBottomNew;
    }

    public int getSpriteTopFinStartTouch(){
        float whaleTopFinFloat = 0.f;
        int whaleTopFinInt = 0;
        whaleTopFinFloat = (float) (getSpriteSheetX() + (getWidthSpriteSheet()/2.5026));//currentX + 187;
        whaleTopFinInt = (int) Math.ceil( whaleTopFinFloat );
        return whaleTopFinInt;
    }

    public int getSpriteTopFinEndTouch(){
        float whaleTopFinFloatEnd = 0.f;
        int whaleTopFinIntEnd = 0;
        whaleTopFinFloatEnd = (float) (getSpriteSheetX() + (getWidthSpriteSheet()/2.08));//currentX + 225;
        whaleTopFinIntEnd = (int) Math.ceil( whaleTopFinFloatEnd );
        return whaleTopFinIntEnd;
    }
}
