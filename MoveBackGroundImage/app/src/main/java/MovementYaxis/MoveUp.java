package MovementYaxis;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.google.android.gms.example.movebackgroundimage.GamePanel;
import com.google.android.gms.example.movebackgroundimage.Gun;

/**
 * Created by sadiq on 9/21/2016.
 */
public class MoveUp {

    private GamePanel gamePanel;
    private Bitmap bitmapUpDownMove;
    private Gun gun;
    private int x, y, gunY, bitMapHeight, bitMapWidth;
    public static int totalHeight = 3;//can not be equal to zero
    boolean isUpMove = false;


    public MoveUp(GamePanel gamePanel, Bitmap bitmap, int x, int y, Gun gun){
        this.gamePanel = gamePanel;
        this.bitmapUpDownMove = bitmap;
        this.x = x;
        this.y = y;
        this.bitMapHeight = bitmap.getHeight();
        this.bitMapWidth = bitmap.getWidth();
        this.gun = gun;
    }

    public void draw(Canvas canvas){
        moveYupDirection();
        moveYdownDirection();
        canvas.drawBitmap(this.bitmapUpDownMove, this.x, this.y, null);
    }

    private void setMoveUpY(int y){
        MoveUp.totalHeight = y;
    }

    public void setMoveUpBoolean(boolean b){
        this.isUpMove = b;
    }

    public void  moveYupDirection(){
        if (GamePanel.isMoveUpDown){
            //if ( MoveUp.totalHeight <  this.gamePanel.getBackgroundTotalHeight() && this.isUpMove ){
            if ( MoveUp.totalHeight <  GamePanel.HEIGHT && this.isUpMove ){
                if ( MoveUp.totalHeight <= 0){
                    GamePanel.isMoveUpDown = false;
                    setMoveUpBoolean(false);
                }else{
                    MoveUp.totalHeight = this.gun.getGunY();
                    gunY = this.gun.getGunY();
                    gunY = gunY - 3;
                    this.gun.setGunY(gunY);
                    setMoveUpY(gunY);
                }
                /*if ( MoveUp.totalHeight <= 0){

                    GamePanel.isMoveUpDown = false;
                    setMoveUpBoolean(false);
                }else  { // move up
                    MoveUp.totalHeight = this.gun.getGunY();
                    gunY = this.gun.getGunY();
                    gunY = gunY - 3;
                    this.gun.setGunY(gunY);


                    setMoveUpY(gunY);
                }*/
            }
        }
    }

    public void  moveYdownDirection(){
        if (GamePanel.isMoveUpDown){
            if ( MoveUp.totalHeight <  this.gamePanel.getBackgroundTotalHeight() && !isUpMove){
                //if ( MoveUp.totalHeight >= 450){ // move down
                if ( MoveUp.totalHeight >= (GamePanel.HEIGHT - GamePanel.fireButtonHeight - gun.getDiverHeightSpriteSheet())){ // move down
                    GamePanel.isMoveUpDown = false;
                    setMoveUpBoolean(true);
                }else  {
                    MoveUp.totalHeight = this.gun.getGunY();
                    gunY = this.gun.getGunY();
                    gunY = gunY + 3;
                    this.gun.setGunY(gunY);
                    setMoveUpY(gunY);
                }
            }
        }
    }

    public int getMoveUpHeight(){
        return this.bitMapHeight;
    }

    public int getMoveUpWidth(){
        return this.bitMapWidth;
    }
}
