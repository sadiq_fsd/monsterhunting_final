package com.google.android.gms.example.movebackgroundimage;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

public class SplashScreen extends AppCompatActivity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    //public static MediaPlayer backGroundSound;//,  mediaPlayerGameOver;//, mediaPlayerStartAfterGameOver;
    //private MediaPlayer backGroundSound1;//,  mediaPlayerGameOver;//, mediaPlayerStartAfterGameOver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);

        //mediaPlayerGameOver = MediaPlayer.create(this, R.raw.splash_and_game_over);
        //mediaPlayerStartAfterGameOver = MediaPlayer.create(this, R.raw.background);//not in use


        /*backGroundSound1 = MediaPlayer.create(this, R.raw.splash_and_game_over);
        backGroundSound1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });
        backGroundSound1.start();*/


        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreen.this, ClickToPaly.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //backGroundSound1.stop();
    }
}
