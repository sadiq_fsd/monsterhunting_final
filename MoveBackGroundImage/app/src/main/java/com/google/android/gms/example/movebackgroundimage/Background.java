package com.google.android.gms.example.movebackgroundimage;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by sadiq on 9/7/2016.
 */
public class Background {

    private Bitmap image;
    int x, y, dx;

    public Background(Bitmap res){
        image = res;
    }

    public void update(){
        x += dx;
        if ( x <- GamePanel.WIDTH ){
            x = 0;
        }
    }

    public void draw(Canvas canvas){
        canvas.drawBitmap(image, x, (GamePanel.HEIGHT - image.getHeight()), null);

        if ( x < 0){
            canvas.drawBitmap(image, x + GamePanel.WIDTH, (GamePanel.HEIGHT - image.getHeight()), null);
        }
    }

    public void setVector(int dx){
        this.dx = dx;
    }

    public int getTotalHeight(){
        return image.getHeight();
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public int getTotalWidth(){
        return image.getWidth();
    }
}
