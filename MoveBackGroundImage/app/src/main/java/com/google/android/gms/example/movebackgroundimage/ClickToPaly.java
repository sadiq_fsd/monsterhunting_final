package com.google.android.gms.example.movebackgroundimage;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import CustomDialogBox.DialogBoxTakeName;
import DatabaseFolder.Dbhandler;

public class ClickToPaly extends Activity implements View.OnClickListener {
    private AdView mAdView;
    private Animation fadeIn;
    private AnimationSet animationSet;


    public static String _userNameBeforeGameStart;

    ImageView play_game;//, close_btn;
    public static MediaPlayer  mediaPlayerFire, power_ups, soundBible, backGroundSound;

    Dbhandler dbhandler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_click_to_paly);

        dbhandler = new Dbhandler(this);
        init();
    }



    private void init(){

        play_game = (ImageView) findViewById(R.id.play_game);
        mAdView = (AdView) findViewById(R.id.adView);
        //close_btn = (ImageView) findViewById(R.id.close_btn);
        //close_btn.setOnClickListener(this);
        play_game.setOnClickListener(this);

//        backGroundSound = new MediaPlayer();
//        backGroundSound = MediaPlayer.create(this, R.raw.splash_and_game_over);
        mediaPlayerFire = MediaPlayer.create(this, R.raw.firing);
        power_ups = MediaPlayer.create(this, R.raw.power_ups);
        soundBible = MediaPlayer.create(this, R.raw.collision);


        //buttonAnimation();

        setAlphaValueForFadeIn();
        setAnimationFadeIn();
        play_game.setAnimation(animationSet);
        play_game.setVisibility(View.VISIBLE);

        bannerAdMob();
    }

    private void setAlphaValueForFadeIn(){
        fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(800);
    }

    private void setAnimationFadeIn(){
        animationSet = new AnimationSet(false);
        animationSet.addAnimation(fadeIn);

    }


    private void bannerAdMob(){
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    /*private void buttonAnimation(){
        TranslateAnimation animation = new TranslateAnimation(0, 600,0, 0);
        animation.setDuration(3000);
        animation.setFillAfter(true);
        play_game.startAnimation(animation);
        //play_game.setVisibility();
    }*/

    /**when activity start two function will call
     * onStart()
     * onResume
     */
    @Override
    protected void onStart() {
        super.onStart();

        backGroundSound = MediaPlayer.create(this, R.raw.splash_and_game_over);
//        backGroundSound.start();
    }

    @Override
    protected void onResume() {
        super.onResume();


        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                backGroundSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        backGroundSound.start();
                    }
                });
                backGroundSound.start();
            }
        }, 3000);


    }

    /**when activity minimize two function will call
     * onPause()
     * onStop
     */
    @Override
    protected void onPause() {
        super.onPause();

        backGroundSound.stop();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }


    /**when activity start three function will call
     * onRestart()
     * onStart()
     * onResume
     */

    @Override
    protected void onRestart() {
        super.onRestart();
        /*
        if ( !backGroundSoundBoolean ){
            backGroundSound.start();
        }*/

    }

    @Override
    public void onClick(View v) {
        int eleId = v.getId();
        switch (eleId){
            case R.id.play_game:



                if ( dbhandler.getScoreRecordCount() <=0){
                    new DialogBoxTakeName(this, "play");
                } else  {
                    backGroundSound = new MediaPlayer();
                    backGroundSound = backGroundSound.create(this, R.raw.background);
                    backGroundSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
//                            backGroundSound.start();
                        }
                    });
//                    backGroundSound.start();
                    Intent i = new Intent(ClickToPaly.this, Game.class);
                    startActivity(i);
                }






                /*SplashScreen.backGroundSound.stop();

                SplashScreen.backGroundSound = new MediaPlayer();
                SplashScreen.backGroundSound = SplashScreen.backGroundSound.create(this, R.raw.background);
                SplashScreen.backGroundSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        SplashScreen.backGroundSound.start();
                    }
                });
                SplashScreen.backGroundSound.start();

                Intent i = new Intent(ClickToPaly.this, Game.class);
                startActivity(i);*/
                break;
            /*case R.id.close_btn:
                //finish();
                new CloseApplicationDialog(this);

                break;*/
        }
    }

    /*public void set_userNameBeforeGameStart(String _userNameBeforeGameStart) {
        this._userNameBeforeGameStart = _userNameBeforeGameStart;
    }

    public String get_userNameBeforeGameStart() {
        return _userNameBeforeGameStart;
    }*/

}
