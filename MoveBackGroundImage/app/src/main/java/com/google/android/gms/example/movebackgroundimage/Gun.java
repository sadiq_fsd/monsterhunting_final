package com.google.android.gms.example.movebackgroundimage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import BulletPack.BulletsPack;
import CustomDialogBox.DialogBoxCustom;
import DirectionPad.DirectionButtons;
import LifePack.LifePacks;
import MySpriteSheet.HammerheadOneSprite;
import MySpriteSheet.HammerheadSprite;
import MySpriteSheet.SharkSmallSprite;
import MySpriteSheet.Sprite;

/**
 * Created by sadiq on 9/8/2016.
 */
public class Gun {
    DirectionButtons directionButtons;


    private static final int BMP_ROWS = 3;
    private static final int BMP_COLUMNS = 3;
    public static boolean LIFE_COUNT_BOOL_ONE = true;
    public static boolean LIFE_COUNT_BOOL_TWO = true;
    public static boolean LIFE_COUNT_BOOL_THREE = true;
    public static boolean LIFE_COUNT_BOOL_FOUR = true;
    public static int LIFE_COUNTER_ENEMY_COLLISION = 0;
    public static int LIFE_COUNTER_FOR_GETTING_LIFE = 0;
    public static int LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 0;

    public static boolean LIFE_PACK_BOOL_ONE = false;
    public static boolean LIFE_PACK_BOOL_TWO = false;
    public static boolean LIFE_PACK_BOOL_THREE = false;

    private boolean lifePackBoolHereOne = true;
    private boolean lifePackBoolHereTwo = true;
    private boolean lifePackBoolHereThree = true;

    private int x = 0;
    private int y = 0;
    private int xSpeed = 1;

    private GamePanel gamePanel;
    private Bitmap bitmap;
    private int currentFrame = 0;

    private Sprite sprite;
    private HammerheadOneSprite hammerheadOneSprite;
    private SharkSmallSprite sharkSmallSprite;
    //private SmallAnglarFishSprite smallAnglarFishSprite;
    private HammerheadSprite hammerheadSprite;

    private int currentFrameY = 0;
    private int width;
    private int height;
    private int abc = 0;
    private int srcY =0;
    private int srcX =0;
    //private int ownY;
    private int diverAndEnemyCollison = 0;

    private BulletsPack bulletsPack;
    private LifePacks lifePacks;

    private int collideValue = 0;
    private Paint alphaPaint = new Paint();

    private int bounceBackCounter =0;

    public Gun(GamePanel gamePanel, Bitmap bitmap, int x, int y, Sprite sprite, BulletsPack bulletsPack,
               LifePacks lifePacks, HammerheadOneSprite hammerheadOneSprite,
               SharkSmallSprite sharkSmallSprite,  HammerheadSprite hammerheadSprite){ //SmallAnglarFishSprite smallAnglarFishSprite,

        this.gamePanel = gamePanel;
        this.bitmap = bitmap;

        //if (!GamePanel.isMoveUpDownGameOver){//for sprite sheet
        this.width = bitmap.getWidth() / BMP_COLUMNS;
        this.height = bitmap.getHeight() / BMP_ROWS;
        //} else if ( GamePanel.isMoveUpDownGameOver ){
        //} else /*if ( GamePanel.isMoveUpDownGameOver )*/{

            //this.width = bitmap.getWidth();
            //this.height = bitmap.getHeight();
        //}

        this.x = x;
        this.y = y;
        this.sprite = sprite;
        this.hammerheadOneSprite = hammerheadOneSprite;
        this.sharkSmallSprite = sharkSmallSprite;
        //this.smallAnglarFishSprite = smallAnglarFishSprite;
        this.hammerheadSprite = hammerheadSprite;

        this.bulletsPack = bulletsPack;
        this.lifePacks = lifePacks;

    }

    private void update() {
        if(currentFrame ==2 || currentFrame ==3)
            currentFrameY++;
        if(currentFrameY ==2 || currentFrameY ==3)
            currentFrameY = currentFrameY % BMP_COLUMNS;

        currentFrame = ++currentFrame % BMP_COLUMNS;
        isCollision();
    }

    private int getDiverLeavingTopSpace(){ //whale
        float diverTopSpace = 0.0f;
        int diverSpaceInt = 0;
        diverTopSpace = (float) (this.y + (this.height/5.867));//y + 15
        diverSpaceInt = (int) Math.ceil( diverTopSpace );
        return diverSpaceInt;
    }

    private int getDiverLeavingBottomSpace(){
        float diverBottomSpace = 0.0f;
        int diverSpaceIntBottom = 0;
        diverBottomSpace = (float) (this.y + this.height - (this.height/8.8));//y + height - 10
        diverSpaceIntBottom = (int) Math.ceil( diverBottomSpace );
        return diverSpaceIntBottom;
    }

    private void diverWhaleCollision(){
        Game.vibrator.vibrate(300);

        collideValue = 1;
        if  ( LIFE_COUNTER_ENEMY_COLLISION <= 19){
            LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
        }

        if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
            LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
            LIFE_COUNT_BOOL_ONE = false;


            if (bounceBackCounter == 0){
                this.sprite.setSpriteSheetX((int) (this.sprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                Sprite.rYValue = this.sprite.getSpriteSheeRandomY();

                if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                    setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                    setGunX(0);
                }

                bounceBackCounter = 1;
            } else {
                this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                Sprite.rYValue = this.gamePanel.getRandomValue();

                if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                    setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                    setGunX(0);
                }

                bounceBackCounter =0;
            }


            //this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            //Sprite.rYValue = this.gamePanel.getRandomValue();
            collideValue = 0;
        } else if ( LIFE_COUNTER_ENEMY_COLLISION == 15 ){

                if  ( LIFE_COUNT_BOOL_TWO ){
                    LIFE_COUNT_BOOL_TWO = false;
                    lifePackBoolHereTwo = true;
                }

                if (bounceBackCounter == 0){
                    this.sprite.setSpriteSheetX((int) (this.sprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                    Sprite.rYValue = this.sprite.getSpriteSheeRandomY();

                    if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                        setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                    } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                        setGunX(0);
                    }

                    bounceBackCounter = 1;
                } else {
                    this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    Sprite.rYValue = this.gamePanel.getRandomValue();

                    if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                        setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                    } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                        setGunX(0);
                    }

                    bounceBackCounter =0;
                }

                //this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                //Sprite.rYValue = this.gamePanel.getRandomValue();
                collideValue = 0;
            } else if ( LIFE_COUNTER_ENEMY_COLLISION == 10){

                    if ( LIFE_COUNT_BOOL_THREE ){
                        LIFE_COUNT_BOOL_THREE = false;
                        lifePackBoolHereOne = true;
                    }

                    if (bounceBackCounter == 0){
                        this.sprite.setSpriteSheetX((int) (this.sprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                        Sprite.rYValue = this.sprite.getSpriteSheeRandomY();

                        if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                            setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                        } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                            setGunX(0);
                        }

                        bounceBackCounter = 1;
                    }else {
                        this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        Sprite.rYValue = this.gamePanel.getRandomValue();

                        if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                            setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                        } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                            setGunX(0);
                        }

                        bounceBackCounter =0;
                    }

                    //this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    //Sprite.rYValue = this.gamePanel.getRandomValue();
                    collideValue = 0;
                } else if ( LIFE_COUNTER_ENEMY_COLLISION == 5  ){

                        if ( LIFE_COUNT_BOOL_FOUR ){
                            LIFE_COUNT_BOOL_FOUR = false;
                            lifePackBoolHereThree = true;
                        }

                        if (bounceBackCounter == 0){
                            this.sprite.setSpriteSheetX((int) (this.sprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                            Sprite.rYValue = this.sprite.getSpriteSheeRandomY();

                            if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                                setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                            } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                                setGunX(0);
                            }

                            bounceBackCounter = 1;
                        } else {
                            this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                            Sprite.rYValue = this.gamePanel.getRandomValue();

                            if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                                setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                            } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                                setGunX(0);
                            }
                            bounceBackCounter =0;
                        }

                        //on going code
                        //this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        //Sprite.rYValue = this.gamePanel.getRandomValue();
                        collideValue = 0;
                    }

        if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
            LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
            LIFE_COUNTER_ENEMY_COLLISION = 0;
            GamePanel.totalChance = 0;
            //GamePanel.scoreValue = 0;
            DialogBoxCustom.isGameOver = true;
        }
    }

    private void diverHammerHeadOneCollision(){
        Game.vibrator.vibrate(300);
        //ClickToPaly.soundBible.start();

        if  ( LIFE_COUNTER_ENEMY_COLLISION <= 19){
            LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
        }

        //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
        if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
            LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
            LIFE_COUNT_BOOL_ONE = false;

            if (bounceBackCounter == 0){
                this.hammerheadOneSprite.setSpriteSheetX((int) (this.hammerheadOneSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                HammerheadOneSprite.rYValue = this.hammerheadOneSprite.getSpriteSheeRandomY();

                if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                    setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                    setGunX(0);
                }

                bounceBackCounter = 1;
            } else {
                this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();

                if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                    setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                    setGunX(0);
                }

                bounceBackCounter =0;
            }

            //this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            //HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
        } else
            //if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 15 ){

                if  ( LIFE_COUNT_BOOL_TWO ){
                    LIFE_COUNT_BOOL_TWO = false;
                    lifePackBoolHereTwo = true;
                }

                if (bounceBackCounter == 0){
                    this.hammerheadOneSprite.setSpriteSheetX((int) (this.hammerheadOneSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                    HammerheadOneSprite.rYValue = this.hammerheadOneSprite.getSpriteSheeRandomY();

                    if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                        setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                    } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                        setGunX(0);
                    }

                    bounceBackCounter = 1;
                } else {
                    this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();

                    if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                        setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                    } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                        setGunX(0);
                    }

                    bounceBackCounter =0;
                }


                //this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                //HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
            } else
                //if ( LIFE_COUNTER_ENEMY_COLLISION == 30){
                if ( LIFE_COUNTER_ENEMY_COLLISION == 10){

                    if ( LIFE_COUNT_BOOL_THREE ){
                        LIFE_COUNT_BOOL_THREE = false;
                        lifePackBoolHereOne = true;
                    }

                    if (bounceBackCounter == 0){
                        this.hammerheadOneSprite.setSpriteSheetX((int) (this.hammerheadOneSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                        HammerheadOneSprite.rYValue = this.hammerheadOneSprite.getSpriteSheeRandomY();

                        if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                            setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                        } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                            setGunX(0);
                        }

                        bounceBackCounter = 1;
                    } else {
                        this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();

                        if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                            setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                        } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                            setGunX(0);
                        }

                        bounceBackCounter =0;
                    }

                    //this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    //HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
                } else
                    //if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){
                    if ( LIFE_COUNTER_ENEMY_COLLISION == 5  ){

                        if ( LIFE_COUNT_BOOL_FOUR ){
                            LIFE_COUNT_BOOL_FOUR = false;
                            lifePackBoolHereThree = true;
                        }

                        if (bounceBackCounter == 0){
                            this.hammerheadOneSprite.setSpriteSheetX((int) (this.hammerheadOneSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                            HammerheadOneSprite.rYValue = this.hammerheadOneSprite.getSpriteSheeRandomY();

                            if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                                setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                            } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                                setGunX(0);
                            }

                            bounceBackCounter = 1;
                        } else {
                            this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                            HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();

                            if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                                setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                            } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                                setGunX(0);
                            }

                            bounceBackCounter =0;
                        }

                        //this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        //HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
                    }

        if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
            LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
            LIFE_COUNTER_ENEMY_COLLISION = 0;
            GamePanel.totalChance = 0;
            //GamePanel.scoreValue = 0;
            DialogBoxCustom.isGameOver = true;
        }
    }

    private void diverHammerHeadCollision(){
        Game.vibrator.vibrate(300);
        //ClickToPaly.soundBible.start();

        if  ( LIFE_COUNTER_ENEMY_COLLISION <= 19){
            LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
        }

        //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
        if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
            LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
            LIFE_COUNT_BOOL_ONE = false;

            if (bounceBackCounter == 0){
                this.hammerheadSprite.setSpriteSheetX((int) (this.hammerheadSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                HammerheadSprite.rYValue = this.hammerheadSprite.getSpriteSheeRandomY();

                if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                    setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                    setGunX(0);
                }

                bounceBackCounter = 1;
            } else {
                this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                HammerheadSprite.rYValue = this.gamePanel.getRandomValue();

                if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                    setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                    setGunX(0);
                }

                bounceBackCounter =0;
            }

            //this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            //HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
        } else
            //if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 15 ){

                if  ( LIFE_COUNT_BOOL_TWO ){
                    LIFE_COUNT_BOOL_TWO = false;
                    lifePackBoolHereTwo = true;
                }

                if (bounceBackCounter == 0){
                    this.hammerheadSprite.setSpriteSheetX((int) (this.hammerheadSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                    HammerheadSprite.rYValue = this.hammerheadSprite.getSpriteSheeRandomY();

                    if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                        setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                    } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                        setGunX(0);
                    }

                    bounceBackCounter = 1;
                } else {
                    this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    HammerheadSprite.rYValue = this.gamePanel.getRandomValue();

                    if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                        setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                    } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                        setGunX(0);
                    }

                    bounceBackCounter =0;
                }

                //this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                //HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
            } else
                //if ( LIFE_COUNTER_ENEMY_COLLISION == 30){
                if ( LIFE_COUNTER_ENEMY_COLLISION == 10){

                    if ( LIFE_COUNT_BOOL_THREE ){
                        LIFE_COUNT_BOOL_THREE = false;
                        lifePackBoolHereOne = true;
                    }

                    if (bounceBackCounter == 0){
                        this.hammerheadSprite.setSpriteSheetX((int) (this.hammerheadSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                        HammerheadSprite.rYValue = this.hammerheadSprite.getSpriteSheeRandomY();

                        if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                            setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                        } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                            setGunX(0);
                        }

                        bounceBackCounter = 1;
                    } else {
                        this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        HammerheadSprite.rYValue = this.gamePanel.getRandomValue();

                        if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                            setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                        } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                            setGunX(0);
                        }

                        bounceBackCounter =0;
                    }

                    //this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    //HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
                } else
                    //if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){
                    if ( LIFE_COUNTER_ENEMY_COLLISION == 5  ){

                        if ( LIFE_COUNT_BOOL_FOUR ){
                            LIFE_COUNT_BOOL_FOUR = false;
                            lifePackBoolHereThree = true;
                        }

                        if (bounceBackCounter == 0){
                            this.hammerheadSprite.setSpriteSheetX((int) (this.hammerheadSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                            HammerheadSprite.rYValue = this.hammerheadSprite.getSpriteSheeRandomY();

                            if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                                setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                            } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                                setGunX(0);
                            }

                            bounceBackCounter = 1;
                        } else {
                            this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                            HammerheadSprite.rYValue = this.gamePanel.getRandomValue();

                            if (getGunX() > (gamePanel.WIDTH/7.973)){ //150
                                setGunX((int) (getGunX() -  (gamePanel.WIDTH/7.973)));
                            } else if  ( getGunX() <=  (gamePanel.WIDTH/7.973) ){
                                setGunX(0);
                            }

                            bounceBackCounter =0;
                        }

                        //this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        //HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
                    }

        //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
        if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
            LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
            LIFE_COUNTER_ENEMY_COLLISION = 0;
            GamePanel.totalChance = 0;
            //GamePanel.scoreValue = 0;
            DialogBoxCustom.isGameOver = true;
        }
    }

    private void diverSharkSmallSprite(){
        Game.vibrator.vibrate(300);
        //ClickToPaly.soundBible.start();

        if  ( LIFE_COUNTER_ENEMY_COLLISION <= 19){
            LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
        }

        //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
        if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
            LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
            LIFE_COUNT_BOOL_ONE = false;

            if (bounceBackCounter == 0){
                this.sharkSmallSprite.setSpriteSheetX((int) (this.sharkSmallSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                SharkSmallSprite.rYValue = this.sharkSmallSprite.getSpriteSheeRandomY();
                bounceBackCounter = 1;
            } else {
                this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
                bounceBackCounter =0;
            }

            //this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
            //SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
        } else
            //if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 15 ){

                if  ( LIFE_COUNT_BOOL_TWO ){
                    LIFE_COUNT_BOOL_TWO = false;
                    lifePackBoolHereTwo = true;
                }

                if (bounceBackCounter == 0){
                    this.sharkSmallSprite.setSpriteSheetX((int) (this.sharkSmallSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                    SharkSmallSprite.rYValue = this.sharkSmallSprite.getSpriteSheeRandomY();
                    bounceBackCounter = 1;
                } else {
                    this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
                    bounceBackCounter =0;
                }

                //this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                //SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
            } else
                //if ( LIFE_COUNTER_ENEMY_COLLISION == 30){
                if ( LIFE_COUNTER_ENEMY_COLLISION == 10){

                    if ( LIFE_COUNT_BOOL_THREE ){
                        LIFE_COUNT_BOOL_THREE = false;
                        lifePackBoolHereOne = true;
                    }

                    if (bounceBackCounter == 0){
                        this.sharkSmallSprite.setSpriteSheetX((int) (this.sharkSmallSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                        SharkSmallSprite.rYValue = this.sharkSmallSprite.getSpriteSheeRandomY();
                        bounceBackCounter = 1;
                    } else {
                        this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
                        bounceBackCounter =0;
                    }

                    //this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    //SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
                } else
                    //if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){
                    if ( LIFE_COUNTER_ENEMY_COLLISION == 5  ){

                        if ( LIFE_COUNT_BOOL_FOUR ){
                            LIFE_COUNT_BOOL_FOUR = false;
                            lifePackBoolHereThree = true;
                        }

                        if (bounceBackCounter == 0){
                            this.sharkSmallSprite.setSpriteSheetX((int) (this.sharkSmallSprite.getSpriteSheetX() + (GamePanel.WIDTH/11.96)));
                            SharkSmallSprite.rYValue = this.sharkSmallSprite.getSpriteSheeRandomY();
                            bounceBackCounter = 1;
                        } else {
                            this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                            SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
                            bounceBackCounter =0;
                        }

                        //this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        //SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
                    }

        //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
        if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
            LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
            LIFE_COUNTER_ENEMY_COLLISION = 0;
            GamePanel.totalChance = 0;
            //GamePanel.scoreValue = 0;
            DialogBoxCustom.isGameOver = true;
        }
    }

    private void isCollision(){
        //whale collision
        if ( //only for top fin of whale fish
                (this.x < (sprite.getSpriteTopFinEndTouch())//sprite.getSpriteSheetX() + 225
                    &&(this.x + this.width) > (sprite.getSpriteTopFinStartTouch())//getSpriteSheetX() + 187
                    && ( getDiverLeavingBottomSpace() ) >= (sprite.getSpriteSheeRandomY() ) //y + height - 10 >= sprite.getSpriteSheeRandomY()
                    && (getDiverLeavingTopSpace()) <=  ( sprite.getSpriteLeavingTopSpaceForFin() )//y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() + 40
                    //&& (getDiverLeavingTopSpace() ) <=  ( sprite.getSpriteLeavingTopSpace() )//y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() + 20
                )
                && !DialogBoxCustom.isGameOver
            ) {
            diverWhaleCollision();
        }
        else if ( //only for whale fish
                /*(this.x < (sprite.getSpriteSheetX() + sprite.getWidthSpriteSheet())
                        &&(this.x + this.width) > sprite.getSpriteSheetX()
                        //&& ( getDiverLeavingBottomSpace() ) >= (sprite.getSpriteLeavingTopSpace() ) //y + height - 10 >= sprite.getSpriteSheeRandomY()
                        && ( getDiverLeavingBottomSpace() ) > (sprite.getSpriteLeavingTopSpaceForFin() ) //y + height - 10 >= sprite.getSpriteSheeRandomY()
                        && (getDiverLeavingTopSpace() ) <=  ( sprite.getSpriteLeavingBottomSpace() )//y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() + 20
                )*/
                (this.x < (sprite.getSpriteSheetX() + sprite.getWidthSpriteSheet())
                        &&(this.x + this.width) > sprite.getSpriteSheetX()
                        && ( getDiverLeavingBottomSpace() ) > (sprite.getSpriteLeavingTopSpaceForFin() ) //y + height - 10 >= sprite.getSpriteSheeRandomY()
                        //&& (getDiverLeavingTopSpace() ) <=  ( sprite.getSpriteSheeRandomY() + sprite.getHeightSpriteSheet() - (sprite.getHeightSpriteSheet()/2.9) )//y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() - 30
                        && (getDiverLeavingTopSpace() ) <=  ( sprite.getSpriteSheeRandomY() + sprite.getHeightSpriteSheet() - (sprite.getHeightSpriteSheet()/2.93) )//y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() - 30
                )
                        && !DialogBoxCustom.isGameOver
                ){
            diverWhaleCollision();
        }
        else if ( //only for bottom whale fin
                (this.x < (sprite.getSpriteSheetX() + ( sprite.getSpriteSheetX()/1.82 ) )//100
                        &&(this.x + this.width) > (sprite.getSpriteSheetX() + ( sprite.getSpriteSheetX()/9.1 ) )//sprite.getSpriteSheetX() + 20
                        //&& (getDiverLeavingTopSpace() ) >=  ( sprite.getSpriteSheeRandomY() + sprite.getHeightSpriteSheet()-(int)(sprite.getHeightSpriteSheet()/3.64))//50//y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() - 50
                        && (getDiverLeavingTopSpace() ) >  ( sprite.getSpriteSheeRandomY() + sprite.getHeightSpriteSheet() - (sprite.getHeightSpriteSheet()/2.93) )//2.2=40//2.9=30//y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() - 50
                        && (getDiverLeavingTopSpace() ) <=  ( sprite.getSpriteSheeRandomY() + sprite.getHeightSpriteSheet() )//y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() + 20
                )&& !DialogBoxCustom.isGameOver
                ){
            diverWhaleCollision();
        }

        //small shark one new
        if ( //only for small shark fish center
                (this.x < (sharkSmallSprite.getSpriteSheetX() + sharkSmallSprite.getWidthSpriteSheet() )
                        &&(this.x + this.width) > (sharkSmallSprite.getSpriteSheetX()  )
                        //&& ( this.y + height ) >= (sharkSmallSprite.getSpriteSheeRandomY() + (sharkSmallSprite.getHeightSpriteSheet()/3.12) )//25 //y + height - 10 >= sprite.getSpriteSheeRandomY() + 40
                        && ( getDiverLeavingBottomSpace() ) >= (sharkSmallSprite.getSpriteSheeRandomY() + (sharkSmallSprite.getHeightSpriteSheet()/3.12) )//25 //y + height - 10 >= sprite.getSpriteSheeRandomY() + 40
                        && (getDiverLeavingTopSpace() ) <=  ( sharkSmallSprite.getSpriteSheeRandomY() + sharkSmallSprite.getHeightSpriteSheet() - (sharkSmallSprite.getHeightSpriteSheet()/3.9) )//20 //y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() + 20
                )&& !DialogBoxCustom.isGameOver
                ){
            diverSharkSmallSprite();
        } else if ( //only for small shark fish top fin
                (this.x < (sharkSmallSprite.getSpriteSheetX() + sharkSmallSprite.getWidthSpriteSheet() )//+ (sharkSmallSprite.getWidthSpriteSheet()/1.981)//110
                        &&(this.x + this.width) > (sharkSmallSprite.getSpriteSheetX() + (sharkSmallSprite.getWidthSpriteSheet()/2.42) )//+ (sharkSmallSprite.getWidthSpriteSheet()/2.42)//90
                        //&& ( this.y + height - (height/2.6 ) ) >= (sharkSmallSprite.getSpriteSheeRandomY()  )//+ (sharkSmallSprite.getHeightSpriteSheet()/3.12)//25 //y + height - 10 >= sprite.getSpriteSheeRandomY() + 40
                        && ( this.y + height - (height/3.9) )  >= (sharkSmallSprite.getSpriteSheeRandomY()  )//+ (sharkSmallSprite.getHeightSpriteSheet()/3.12)//25 //y + height - 10 >= sprite.getSpriteSheeRandomY() + 40
                        && (getDiverLeavingTopSpace() ) <=  ( sharkSmallSprite.getSpriteSheeRandomY() + sharkSmallSprite.getHeightSpriteSheet() - (sharkSmallSprite.getHeightSpriteSheet()/3.9) )//20 //y + 15 <= sprite.getSpriteSheeRandomY()+ sprite.getHeightSpriteSheet() + 20
                )&& !DialogBoxCustom.isGameOver
                ){
            diverSharkSmallSprite();
        } else if ( //only for small shark fish bottom fin
                (this.x < (sharkSmallSprite.getSpriteSheetX() + sharkSmallSprite.getWidthSpriteSheet() )//+ (sharkSmallSprite.getWidthSpriteSheet()/1.981)//110
                        &&(this.x + this.width) > (sharkSmallSprite.getSpriteSheetX() + (sharkSmallSprite.getWidthSpriteSheet()/2.42) )//+ (sharkSmallSprite.getWidthSpriteSheet()/2.42)//90
                        //&& ( this.y + height ) >= (sharkSmallSprite.getSpriteSheeRandomY()  )//+ (sharkSmallSprite.getHeightSpriteSheet()/3.12)//25 //y + height - 10 >= sprite.getSpriteSheeRandomY() + 40
                        && (getDiverLeavingTopSpace() ) >=  ( sharkSmallSprite.getSpriteSheeRandomY() + sharkSmallSprite.getHeightSpriteSheet() - (sharkSmallSprite.getHeightSpriteSheet()/3.9) )//20
                        && (getDiverLeavingTopSpace() ) <=  ( sharkSmallSprite.getSpriteSheeRandomY() + sharkSmallSprite.getHeightSpriteSheet() - (sharkSmallSprite.getHeightSpriteSheet()/7.8) )//10
                )&& !DialogBoxCustom.isGameOver
                ){
            diverSharkSmallSprite();
        }

        //hammaer head grey fish spritecollision
        if ( //only for hammerhead fish center
                (this.x < (hammerheadOneSprite.getSpriteSheetX() + hammerheadOneSprite.getWidthSpriteSheet() )
                        &&(this.x + this.width) > (hammerheadOneSprite.getSpriteSheetX()  )
                        && (y + height ) >= (hammerheadOneSprite.getSpriteSheeRandomY() + (hammerheadOneSprite.getHeightSpriteSheet()/3.08 ))//25
                        && (y) <= (hammerheadOneSprite.getSpriteSheeRandomY() + (hammerheadOneSprite.getHeightSpriteSheet()/1.925 ))//40


                        //&& (getDiverLeavingTopSpace())  <= (hammerheadOneSprite.getSpriteSheeRandomY() + hammerheadOneSprite.getHeightSpriteSheet() - (hammerheadOneSprite.getHeightSpriteSheet()/1.925 ))//40
                )&& !DialogBoxCustom.isGameOver
                ){
            diverHammerHeadOneCollision();



        } else if ( //only for hammerheadOneSprite Sprite fish top fin
                (this.x < (hammerheadOneSprite.getSpriteSheetX() + hammerheadOneSprite.getWidthSpriteSheet() )
                        &&(this.x + this.width) > (hammerheadOneSprite.getSpriteSheetX() + (hammerheadOneSprite.getWidthSpriteSheet()/4.04)  )//50 //+(hammerheadOneSprite.getWidthSpriteSheet()/2.9)=80
                        && ( this.y + height - (height/3.85) ) >= (hammerheadOneSprite.getSpriteSheeRandomY()  )//20
                        && ( getDiverLeavingTopSpace() ) <= ( hammerheadOneSprite.getSpriteSheeRandomY() + (hammerheadOneSprite.getHeightSpriteSheet()/3.08) )//25   getDiverLeavingTopSpace()
                        ////&& ( this.y + height - (height/3.9) ) >= (hammerheadOneSprite.getSpriteSheeRandomY()  )//20
                        ////&& (getDiverLeavingTopSpace() ) <=  ( hammerheadOneSprite.getSpriteSheeRandomY() + (hammerheadOneSprite.getHeightSpriteSheet()/ 2.51) )//35
                        //&& (getDiverLeavingTopSpace() ) <=  ( hammerheadOneSprite.getSpriteSheeRandomY() + hammerheadOneSprite.getHeightSpriteSheet() -  (hammerheadOneSprite.getWidthSpriteSheet()/5.8 ) )//40
                )&& !DialogBoxCustom.isGameOver
                ){

            diverHammerHeadOneCollision();
        } else if ( //only for hammerheadOneSprite fish bottom fin
                (this.x < (hammerheadOneSprite.getSpriteSheetX() + hammerheadOneSprite.getWidthSpriteSheet() )//+ (hammerheadOneSprite.getWidthSpriteSheet()/1.981)//110
                        &&(this.x + this.width) > (hammerheadOneSprite.getSpriteSheetX() + ( hammerheadOneSprite.getWidthSpriteSheet() / 6.73) )// + 30//+ (hammerheadOneSprite.getWidthSpriteSheet()/2.42)//90
                        && (getDiverLeavingTopSpace() ) >=  ( hammerheadOneSprite.getSpriteSheeRandomY() + hammerheadOneSprite.getHeightSpriteSheet() - (hammerheadOneSprite.getHeightSpriteSheet()/ 1.925) )//40
                        && (getDiverLeavingTopSpace() ) <= ( hammerheadOneSprite.getSpriteSheeRandomY() + hammerheadOneSprite.getHeightSpriteSheet() - (hammerheadOneSprite.getHeightSpriteSheet()/ 15.4 ) )//5
                )&& !DialogBoxCustom.isGameOver
                ){
            diverHammerHeadOneCollision();

        }

        //hammaer head brown sprite
        if ( //only for hammerhead fish center
                (this.x < (hammerheadSprite.getSpriteSheetX() + hammerheadSprite.getWidthSpriteSheet() )
                        &&(this.x + this.width) > (hammerheadSprite.getSpriteSheetX()  )
                        && (y + height - (height/3.9)) >= (hammerheadSprite.getSpriteSheeRandomY() + (hammerheadSprite.getWidthSpriteSheet()/5.8 )))//40
                        && (getDiverLeavingTopSpace())  <= (hammerheadSprite.getSpriteSheeRandomY() + hammerheadSprite.getHeightSpriteSheet() - (hammerheadSprite.getHeightSpriteSheet()/4.4)//20
                )&& !DialogBoxCustom.isGameOver
                ){
            diverHammerHeadCollision();

        } else if ( //only for hammerhead Sprite fish top fin
                (this.x < (hammerheadSprite.getSpriteSheetX() + hammerheadSprite.getWidthSpriteSheet() )
                        &&(this.x + this.width) > (hammerheadSprite.getSpriteSheetX() +(hammerheadSprite.getWidthSpriteSheet()/2.9) )//80
                        && ( this.y + height - (height/8.8) ) >= (hammerheadSprite.getSpriteSheeRandomY()  )//20
                        //&& ( this.y + height - (height/3.9) ) >= (hammerheadSprite.getSpriteSheeRandomY()  )//20
                        && (getDiverLeavingTopSpace() ) <=  ( hammerheadSprite.getSpriteSheeRandomY() + hammerheadSprite.getHeightSpriteSheet() -  (hammerheadSprite.getWidthSpriteSheet()/5.8 ) )//40
                        //&& (getDiverLeavingTopSpace() ) <=  ( hammerheadSprite.getSpriteSheeRandomY() + (hammerheadSprite.getHeightSpriteSheet()/ 2.51) )//35
                )&& !DialogBoxCustom.isGameOver
                ){
            diverHammerHeadCollision();
        } else if ( //only for small shark fish top fin
                (this.x < (hammerheadSprite.getSpriteSheetX() + hammerheadSprite.getWidthSpriteSheet() )//+ (hammerheadSprite.getWidthSpriteSheet()/1.981)//110
                        &&(this.x + this.width) > (hammerheadSprite.getSpriteSheetX() + ( hammerheadSprite.getWidthSpriteSheet() / 2.9) )// + 80//+ (hammerheadSprite.getWidthSpriteSheet()/2.42)//90
                        && (getDiverLeavingTopSpace() ) >=  ( hammerheadSprite.getSpriteSheeRandomY() + hammerheadSprite.getHeightSpriteSheet() - (hammerheadSprite.getHeightSpriteSheet()/ 4.4) )//20
                        && (getDiverLeavingTopSpace() ) <= ( hammerheadSprite.getSpriteSheeRandomY() + hammerheadSprite.getHeightSpriteSheet() - (hammerheadSprite.getHeightSpriteSheet()/ 17.6 ) )//5
                )&& !DialogBoxCustom.isGameOver
                ){
            diverHammerHeadCollision();
        }

        /****5 fishes collision  workin code ****/

        //enemy and diver collision
        /*if (
                (this.x < (sprite.getSpriteSheetX() + sprite.getWidthSpriteSheet())
                        && (this.x + this.width) > sprite.getSpriteSheetX()
                        && (this.y +this.height) >= (sprite.getSpriteSheeRandomY())
                        && (this.y ) <= (sprite.getSpriteSheeRandomY() + this.sprite.getHeightSpriteSheet() ) )
                        && !DialogBoxCustom.isGameOver
                ){

            Game.vibrator.vibrate(300);
            ClickToPaly.soundBible.start();

            if  ( LIFE_COUNTER_ENEMY_COLLISION <= 19){
                LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
            }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNT_BOOL_ONE = false;
                this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                Sprite.rYValue = this.gamePanel.getRandomValue();
            } else
            //if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 15 ){

                if  ( LIFE_COUNT_BOOL_TWO ){
                    LIFE_COUNT_BOOL_TWO = false;
                    lifePackBoolHereTwo = true;
                }
                this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                Sprite.rYValue = this.gamePanel.getRandomValue();
            } else
            //if ( LIFE_COUNTER_ENEMY_COLLISION == 30){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 10){

                if ( LIFE_COUNT_BOOL_THREE ){
                    LIFE_COUNT_BOOL_THREE = false;
                    lifePackBoolHereOne = true;
                }

                this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                Sprite.rYValue = this.gamePanel.getRandomValue();
            } else
            //if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 5  ){

                if ( LIFE_COUNT_BOOL_FOUR ){
                    LIFE_COUNT_BOOL_FOUR = false;
                    lifePackBoolHereThree = true;
                }

                this.sprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                Sprite.rYValue = this.gamePanel.getRandomValue();
            }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNTER_ENEMY_COLLISION = 0;
                GamePanel.totalChance = 0;
                GamePanel.scoreValue = 0;
                DialogBoxCustom.isGameOver = true;
            }
        }*/

        //hammerheadONeSprite
        /*if ( (this.x + this.width) > hammerheadOneSprite.getSpriteSheetX()
                && (this.x) < (hammerheadOneSprite.getSpriteSheetX() + hammerheadOneSprite.getWidthSpriteSheet())
                && (this.y +this.height) >= (hammerheadOneSprite.getSpriteSheeRandomY())
                && (this.y ) <= (hammerheadOneSprite.getSpriteSheeRandomY() + this.hammerheadOneSprite.getHeightSpriteSheet() )
                && !DialogBoxCustom.isGameOver
                ){

            Game.vibrator.vibrate(300);
            ClickToPaly.soundBible.start();

            if  ( LIFE_COUNTER_ENEMY_COLLISION <= 19){
                LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
            }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNT_BOOL_ONE = false;
                this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
            } else
                //if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){
                if ( LIFE_COUNTER_ENEMY_COLLISION == 15 ){

                    if  ( LIFE_COUNT_BOOL_TWO ){
                        LIFE_COUNT_BOOL_TWO = false;
                        lifePackBoolHereTwo = true;
                    }
                    this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
                } else
                    //if ( LIFE_COUNTER_ENEMY_COLLISION == 30){
                    if ( LIFE_COUNTER_ENEMY_COLLISION == 10){

                        if ( LIFE_COUNT_BOOL_THREE ){
                            LIFE_COUNT_BOOL_THREE = false;
                            lifePackBoolHereOne = true;
                        }

                        this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
                    } else
                        //if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){
                        if ( LIFE_COUNTER_ENEMY_COLLISION == 5  ){

                            if ( LIFE_COUNT_BOOL_FOUR ){
                                LIFE_COUNT_BOOL_FOUR = false;
                                lifePackBoolHereThree = true;
                            }

                            this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                            HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
                        }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNTER_ENEMY_COLLISION = 0;
                GamePanel.totalChance = 0;
                GamePanel.scoreValue = 0;
                DialogBoxCustom.isGameOver = true;
            }

        }*/

        //hammerheadSprite
        /*if ( (this.x + this.width) > hammerheadSprite.getSpriteSheetX()
                && (this.x) < (hammerheadSprite.getSpriteSheetX() + hammerheadSprite.getWidthSpriteSheet())
                && (this.y +this.height) >= (hammerheadSprite.getSpriteSheeRandomY())
                && (this.y ) <= (hammerheadSprite.getSpriteSheeRandomY() + this.hammerheadSprite.getHeightSpriteSheet() )
                && !DialogBoxCustom.isGameOver
                ){

            Game.vibrator.vibrate(300);
            ClickToPaly.soundBible.start();

            if  ( LIFE_COUNTER_ENEMY_COLLISION <= 19){
                LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
            }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNT_BOOL_ONE = false;
                this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
            } else
                //if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){
                if ( LIFE_COUNTER_ENEMY_COLLISION == 15 ){

                    if  ( LIFE_COUNT_BOOL_TWO ){
                        LIFE_COUNT_BOOL_TWO = false;
                        lifePackBoolHereTwo = true;
                    }
                    this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
                } else
                    //if ( LIFE_COUNTER_ENEMY_COLLISION == 30){
                    if ( LIFE_COUNTER_ENEMY_COLLISION == 10){

                        if ( LIFE_COUNT_BOOL_THREE ){
                            LIFE_COUNT_BOOL_THREE = false;
                            lifePackBoolHereOne = true;
                        }

                        this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
                    } else
                        //if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){
                        if ( LIFE_COUNTER_ENEMY_COLLISION == 5  ){

                            if ( LIFE_COUNT_BOOL_FOUR ){
                                LIFE_COUNT_BOOL_FOUR = false;
                                lifePackBoolHereThree = true;
                            }

                            this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                            HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
                        }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNTER_ENEMY_COLLISION = 0;
                GamePanel.totalChance = 0;
                GamePanel.scoreValue = 0;
                DialogBoxCustom.isGameOver = true;
            }
        }*/

        //smallAnglarFishSprite collision
        /*if ( (this.x + this.width) > smallAnglarFishSprite.getSpriteSheetX()
                && (this.x) < (smallAnglarFishSprite.getSpriteSheetX() + smallAnglarFishSprite.getWidthSpriteSheet())
                && (this.y +this.height) >= (smallAnglarFishSprite.getSpriteSheeRandomY())
                && (this.y ) <= (smallAnglarFishSprite.getSpriteSheeRandomY() + this.smallAnglarFishSprite.getHeightSpriteSheet() )
                && !DialogBoxCustom.isGameOver
                ){

            Game.vibrator.vibrate(300);
            ClickToPaly.soundBible.start();

            if  ( LIFE_COUNTER_ENEMY_COLLISION <= 19){
                LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
            }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNT_BOOL_ONE = false;
                this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                SmallAnglarFishSprite.rYValue = this.gamePanel.getRandomValue();
            } else
                //if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){
                if ( LIFE_COUNTER_ENEMY_COLLISION == 15 ){

                    if  ( LIFE_COUNT_BOOL_TWO ){
                        LIFE_COUNT_BOOL_TWO = false;
                        lifePackBoolHereTwo = true;
                    }
                    this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    SmallAnglarFishSprite.rYValue = this.gamePanel.getRandomValue();
                } else
                    //if ( LIFE_COUNTER_ENEMY_COLLISION == 30){
                    if ( LIFE_COUNTER_ENEMY_COLLISION == 10){

                        if ( LIFE_COUNT_BOOL_THREE ){
                            LIFE_COUNT_BOOL_THREE = false;
                            lifePackBoolHereOne = true;
                        }

                        this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        SmallAnglarFishSprite.rYValue = this.gamePanel.getRandomValue();
                    } else
                        //if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){
                        if ( LIFE_COUNTER_ENEMY_COLLISION == 5  ){

                            if ( LIFE_COUNT_BOOL_FOUR ){
                                LIFE_COUNT_BOOL_FOUR = false;
                                lifePackBoolHereThree = true;
                            }

                            this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                            SmallAnglarFishSprite.rYValue = this.gamePanel.getRandomValue();
                        }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNTER_ENEMY_COLLISION = 0;
                GamePanel.totalChance = 0;
                GamePanel.scoreValue = 0;
                DialogBoxCustom.isGameOver = true;
            }
        }*/

        //collision b/w sharkSmallSprite and diver
        /*if ( (this.x + this.width) > sharkSmallSprite.getSpriteSheetX()
                && (this.x) < (sharkSmallSprite.getSpriteSheetX() + sharkSmallSprite.getWidthSpriteSheet())
                && (this.y +this.height) >= (sharkSmallSprite.getSpriteSheeRandomY())
                && (this.y ) <= (sharkSmallSprite.getSpriteSheeRandomY() + this.sharkSmallSprite.getHeightSpriteSheet() )
                && !DialogBoxCustom.isGameOver
                ){

            Game.vibrator.vibrate(300);
            ClickToPaly.soundBible.start();

            if  ( LIFE_COUNTER_ENEMY_COLLISION <= 19){
                LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
            }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNT_BOOL_ONE = false;
                this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
            } else
                //if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){
                if ( LIFE_COUNTER_ENEMY_COLLISION == 15 ){

                    if  ( LIFE_COUNT_BOOL_TWO ){
                        LIFE_COUNT_BOOL_TWO = false;
                        lifePackBoolHereTwo = true;
                    }
                    this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                    SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
                } else
                    //if ( LIFE_COUNTER_ENEMY_COLLISION == 30){
                    if ( LIFE_COUNTER_ENEMY_COLLISION == 10){

                        if ( LIFE_COUNT_BOOL_THREE ){
                            LIFE_COUNT_BOOL_THREE = false;
                            lifePackBoolHereOne = true;
                        }

                        this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                        SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
                    } else
                        //if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){
                        if ( LIFE_COUNTER_ENEMY_COLLISION == 5  ){

                            if ( LIFE_COUNT_BOOL_FOUR ){
                                LIFE_COUNT_BOOL_FOUR = false;
                                lifePackBoolHereThree = true;
                            }

                            this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 300 );
                            SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
                        }

            //if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
            if ( LIFE_COUNTER_ENEMY_COLLISION == 20 ){
                LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 20;
                LIFE_COUNTER_ENEMY_COLLISION = 0;
                GamePanel.totalChance = 0;
                GamePanel.scoreValue = 0;
                DialogBoxCustom.isGameOver = true;
            }
        }*/

        //================
        //collision b/w Lifepack and diver
        if ( this.x + width >= lifePacks.getLifePackX()
                && this.x <= lifePacks.getLifePackX() + lifePacks.getWidthLifePack()
                && (this.y +this.height) >= (lifePacks.getLifePackY())
                && this.y <= lifePacks.getLifePackY() + lifePacks.getHeightLifePack()
                ) {
            if (!DialogBoxCustom.isGameOver) {


                //ClickToPaly.power_ups.start();

                int randomYAxisForLife = 0;
                randomYAxisForLife = gamePanel.getRandomValue();


                if (LIFE_COUNTER_FOR_GETTING_LIFE <= 4 && LIFE_COUNTER_ENEMY_COLLISION != 0) {
                    LIFE_COUNTER_FOR_GETTING_LIFE = LIFE_COUNTER_FOR_GETTING_LIFE + 1;
                }


                //when one helth decrease from right slide
                if (LIFE_COUNTER_FOR_GETTING_LIFE == 5 && (LIFE_COUNTER_ENEMY_COLLISION > 0 && LIFE_COUNTER_ENEMY_COLLISION <= 5)) { //&& lifePackBoolHereThree
                    ClickToPaly.power_ups.start();
                    if (lifePackBoolHereThree) {
                        //ClickToPaly.power_ups.start();

                        Game.vibrator.vibrate(100);

                        MainThread.healthTimerCounter = 0;

                        LIFE_PACK_BOOL_THREE = true;
                        LIFE_COUNTER_ENEMY_COLLISION = 0;
                        LIFE_COUNTER_FOR_GETTING_LIFE = 0;

                        if (!LIFE_COUNT_BOOL_FOUR)
                            LIFE_COUNT_BOOL_FOUR = true;

                        lifePacks.setLifePackX(GamePanel.WIDTH);
                        lifePacks.setLifePackY(randomYAxisForLife);

                        lifePackBoolHereThree = false;
                    } else {

                        LIFE_PACK_BOOL_THREE = false;
                    }
                }//when two healths decrease from right slide
                else if (LIFE_COUNTER_FOR_GETTING_LIFE == 5 && (LIFE_COUNTER_ENEMY_COLLISION > 5 && LIFE_COUNTER_ENEMY_COLLISION <= 10)//6 to 10
                        || LIFE_COUNTER_FOR_GETTING_LIFE == 0 && (LIFE_COUNTER_ENEMY_COLLISION > 5 && LIFE_COUNTER_ENEMY_COLLISION <= 10)) {

                    ClickToPaly.power_ups.start();

                    if (lifePackBoolHereOne) {
                        Game.vibrator.vibrate(100);
                        MainThread.healthTimerCounter = 0;

                        LIFE_PACK_BOOL_ONE = true;
                        LIFE_COUNTER_ENEMY_COLLISION = 5;

                        LIFE_COUNTER_FOR_GETTING_LIFE = 0;

                        if (!LIFE_COUNT_BOOL_THREE)
                            LIFE_COUNT_BOOL_THREE = true;

                        lifePacks.setLifePackX(GamePanel.WIDTH);
                        lifePacks.setLifePackY(randomYAxisForLife);

                        lifePackBoolHereOne = false;
                    } else {
                        LIFE_PACK_BOOL_ONE = false;
                    }
                }//when three healths decrease from right slide
                else if (LIFE_COUNTER_FOR_GETTING_LIFE == 5 && (LIFE_COUNTER_ENEMY_COLLISION > 10 && LIFE_COUNTER_ENEMY_COLLISION <= 20)) {//15
                    ClickToPaly.power_ups.start();

                    if (lifePackBoolHereTwo) {
                        Game.vibrator.vibrate(100);
                        MainThread.healthTimerCounter = 0;

                        LIFE_PACK_BOOL_TWO = true;
                        LIFE_COUNTER_ENEMY_COLLISION = 10;
                        LIFE_COUNTER_FOR_GETTING_LIFE = 0;

                        if (!LIFE_COUNT_BOOL_TWO)
                            LIFE_COUNT_BOOL_TWO = true;

                        lifePacks.setLifePackX(GamePanel.WIDTH);
                        lifePacks.setLifePackY(randomYAxisForLife);

                        lifePackBoolHereTwo = false;
                    } else {
                        LIFE_PACK_BOOL_TWO = false;
                    }
                }

            /*if ( LIFE_COUNTER_FOR_GETTING_LIFE < 15 && LIFE_COUNTER_ENEMY_COLLISION != 0 ){
                LIFE_COUNTER_FOR_GETTING_LIFE = LIFE_COUNTER_FOR_GETTING_LIFE + 1;
            }*/

           /*


            //when one helth decrease from right slide
            if ( LIFE_COUNTER_FOR_GETTING_LIFE == 15 && (LIFE_COUNTER_ENEMY_COLLISION > 0 && LIFE_COUNTER_ENEMY_COLLISION <= 15)  ){//&& lifePackBoolHereThree


                if ( lifePackBoolHereThree ){
                    LIFE_PACK_BOOL_THREE = true;
                    LIFE_COUNTER_ENEMY_COLLISION = 0;

                    LIFE_COUNTER_FOR_GETTING_LIFE = 0;

                    if ( !LIFE_COUNT_BOOL_FOUR )
                        LIFE_COUNT_BOOL_FOUR = true;

                    lifePacks.setLifePackX( GamePanel.WIDTH );
                    lifePacks.setLifePackY(randomYAxisForLife);

                    lifePackBoolHereThree = false;
                } else {
                    LIFE_PACK_BOOL_THREE = false;
                }
            } //when two healths decrease from right slide
            else if (LIFE_COUNTER_FOR_GETTING_LIFE == 15 && (LIFE_COUNTER_ENEMY_COLLISION >= 16 && LIFE_COUNTER_ENEMY_COLLISION <= 30)
                    || LIFE_COUNTER_FOR_GETTING_LIFE == 0 && (LIFE_COUNTER_ENEMY_COLLISION >= 16 && LIFE_COUNTER_ENEMY_COLLISION <= 30)  ){


                if ( lifePackBoolHereOne ){
                    LIFE_PACK_BOOL_ONE = true;
                    LIFE_COUNTER_ENEMY_COLLISION = 15;

                    LIFE_COUNTER_FOR_GETTING_LIFE = 0;

                    if(!LIFE_COUNT_BOOL_THREE)
                        LIFE_COUNT_BOOL_THREE = true;

                    lifePacks.setLifePackX( GamePanel.WIDTH );
                    lifePacks.setLifePackY(randomYAxisForLife);

                    lifePackBoolHereOne = false;
                } else {
                    LIFE_PACK_BOOL_ONE = false;
                }
            }
            else if (LIFE_COUNTER_FOR_GETTING_LIFE == 15 && (LIFE_COUNTER_ENEMY_COLLISION >= 31 && LIFE_COUNTER_ENEMY_COLLISION <= 60) ){
                if ( lifePackBoolHereTwo ){
                    LIFE_PACK_BOOL_TWO = true;
                    LIFE_COUNTER_ENEMY_COLLISION = 30;

                    LIFE_COUNTER_FOR_GETTING_LIFE = 0;

                    if ( !LIFE_COUNT_BOOL_TWO )
                        LIFE_COUNT_BOOL_TWO = true;

                    lifePacks.setLifePackX( GamePanel.WIDTH );
                    lifePacks.setLifePackY(randomYAxisForLife);

                    lifePackBoolHereTwo = false;
                } else {
                    LIFE_PACK_BOOL_TWO = false;
                }
            }*/
            }
        }

        //collision b/w diver and hammerheadOneSprite
        /*if ( (this.x + this.width) > hammerheadOneSprite.getSpriteSheetX()
                && (this.x) < (hammerheadOneSprite.getSpriteSheetX() + hammerheadOneSprite.getWidthSpriteSheet())
                && (this.y +this.height) >= (hammerheadOneSprite.getSpriteSheeRandomY())
                && (this.y ) <= (hammerheadOneSprite.getSpriteSheeRandomY() + this.hammerheadOneSprite.getHeightSpriteSheet() )
                ){

            //Game.vibrator.vibrate(500);

            LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
            if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){

                LIFE_COUNT_BOOL_ONE = false;

                this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){

                LIFE_COUNT_BOOL_TWO = false;

                this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 30){

                LIFE_COUNT_BOOL_THREE = false;

                this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){

                LIFE_COUNT_BOOL_FOUR = false;

                this.hammerheadOneSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                HammerheadOneSprite.rYValue = this.gamePanel.getRandomValue();
            }

            if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
                LIFE_COUNTER_ENEMY_COLLISION = 0;
                GamePanel.totalChance = 0;
                GamePanel.scoreValue = 0;
                DialogBoxCustom.isGameOver = true;

            }

        }
        //collision b/w sharkSmallSprite and diver
        if ( (this.x + this.width) > sharkSmallSprite.getSpriteSheetX()
                && (this.x) < (sharkSmallSprite.getSpriteSheetX() + sharkSmallSprite.getWidthSpriteSheet())
                && (this.y +this.height) >= (sharkSmallSprite.getSpriteSheeRandomY())
                && (this.y ) <= (sharkSmallSprite.getSpriteSheeRandomY() + this.sharkSmallSprite.getHeightSpriteSheet() )
                ){

            //Game.vibrator .vibrate(500);

            LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
            if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){

                LIFE_COUNT_BOOL_ONE = false;

                this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){

                LIFE_COUNT_BOOL_TWO = false;

                this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 30){

                LIFE_COUNT_BOOL_THREE = false;

                this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){

                LIFE_COUNT_BOOL_FOUR = false;

                this.sharkSmallSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                SharkSmallSprite.rYValue = this.gamePanel.getRandomValue();
            }

            if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
                LIFE_COUNTER_ENEMY_COLLISION = 0;
                GamePanel.totalChance = 0;
                GamePanel.scoreValue = 0;
                DialogBoxCustom.isGameOver = true;

            }

        }

        //collision b/w smallAnglarFishSprite and diver
        if ( (this.x + this.width) > smallAnglarFishSprite.getSpriteSheetX()
                && (this.x) < (smallAnglarFishSprite.getSpriteSheetX() + smallAnglarFishSprite.getWidthSpriteSheet())
                && (this.y +this.height) >= (smallAnglarFishSprite.getSpriteSheeRandomY())
                && (this.y ) <= (smallAnglarFishSprite.getSpriteSheeRandomY() + this.smallAnglarFishSprite.getHeightSpriteSheet() )
                ){

            //Game.vibrator.vibrate(500);

            LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
            if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){

                LIFE_COUNT_BOOL_ONE = false;

                this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                SmallAnglarFishSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){

                LIFE_COUNT_BOOL_TWO = false;

                this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                SmallAnglarFishSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 30){

                LIFE_COUNT_BOOL_THREE = false;

                this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                SmallAnglarFishSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){

                LIFE_COUNT_BOOL_FOUR = false;

                this.smallAnglarFishSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                SmallAnglarFishSprite.rYValue = this.gamePanel.getRandomValue();
            }

            if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
                LIFE_COUNTER_ENEMY_COLLISION = 0;
                GamePanel.totalChance = 0;
                GamePanel.scoreValue = 0;
                DialogBoxCustom.isGameOver = true;

            }

        }
        //hammerheadSprite
        if ( (this.x + this.width) > hammerheadSprite.getSpriteSheetX()
                && (this.x) < (hammerheadSprite.getSpriteSheetX() + hammerheadSprite.getWidthSpriteSheet())
                && (this.y +this.height) >= (hammerheadSprite.getSpriteSheeRandomY())
                && (this.y ) <= (hammerheadSprite.getSpriteSheeRandomY() + this.hammerheadSprite.getHeightSpriteSheet() )
                ){

            //Game.vibrator.vibrate(500);

            LIFE_COUNTER_ENEMY_COLLISION = LIFE_COUNTER_ENEMY_COLLISION + 1;
            if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){

                LIFE_COUNT_BOOL_ONE = false;

                this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 45 ){

                LIFE_COUNT_BOOL_TWO = false;

                this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 30){

                LIFE_COUNT_BOOL_THREE = false;

                this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
            } else
            if ( LIFE_COUNTER_ENEMY_COLLISION == 15  ){

                LIFE_COUNT_BOOL_FOUR = false;


                this.hammerheadSprite.setSpriteSheetX(GamePanel.WIDTH + 200 );
                HammerheadSprite.rYValue = this.gamePanel.getRandomValue();
            }

            if ( LIFE_COUNTER_ENEMY_COLLISION == 60 ){
                LIFE_COUNTER_ENEMY_COLLISION = 0;
                GamePanel.totalChance = 0;
                GamePanel.scoreValue = 0;
                DialogBoxCustom.isGameOver = true;

            }
        }*/

        //collision b/w bulletspack and diver
        if ( this.x + width >= bulletsPack.getBulletPackX()
                && this.x <= bulletsPack.getBulletPackX() + bulletsPack.getWidthBulletPack()
                && (this.y +this.height) >= (bulletsPack.getBulletPackY())
                && this.y <= bulletsPack.getBulletPackY() + bulletsPack.getHeightBulletPack()
                ) {

            if (!DialogBoxCustom.isGameOver) {


                //ClickToPaly.power_ups.start();
                //ClickToPaly.mediaPlayerBackground.stop();

                int randomYAxisForLife = 0;
                randomYAxisForLife = gamePanel.getRandomValue();


                if (GamePanel.totalChance >= 0 && GamePanel.totalChance <= 40) {
                    ClickToPaly.power_ups.start();
                    Game.vibrator.vibrate(100);

                    bulletsPack.setBulletPackX(GamePanel.WIDTH + 150);
                    bulletsPack.setBulletPackY(randomYAxisForLife);

                    MainThread.bulletTimerCounter = 0;
                    GamePanel.totalChance = GamePanel.totalChance + BulletsPack.RANDOM_BULLETS_VALUE;
                } else if (GamePanel.totalChance >= 41 && GamePanel.totalChance <= 46) {
                    ClickToPaly.power_ups.start();
                    Game.vibrator.vibrate(100);

                    bulletsPack.setBulletPackX(GamePanel.WIDTH + 150);
                    bulletsPack.setBulletPackY(randomYAxisForLife);

                    MainThread.bulletTimerCounter = 0;
                    GamePanel.totalChance = GamePanel.totalChance + 2;
                } else if (GamePanel.totalChance >= 47 && GamePanel.totalChance <= 49) {
                    ClickToPaly.power_ups.start();
                    Game.vibrator.vibrate(100);

                    bulletsPack.setBulletPackX(GamePanel.WIDTH + 150);
                    bulletsPack.setBulletPackY(randomYAxisForLife);

                    MainThread.bulletTimerCounter = 0;
                    GamePanel.totalChance = GamePanel.totalChance + 1;
                }

            /*if ( GamePanel.totalChance > 50){
                GamePanel.totalChance = GamePanel.totalChance - 1
            }*/
            }
        }
    }

    public void draw(Canvas canvas){
        update();

        /*if ( !GamePanel.isMoveUpDownGameOver  ){*/
            //xPlusWidth();
            //srcX = currentFrame * width;
            //srcY = currentFrameY * height;
            if (abc == 0 ){
                //srcY = 0 * height;
                srcX = currentFrame * width;
                srcY = currentFrameY * height;
                abc = abc +1;
            } else if ( abc == 1){
                //srcY = 1 * height;
                srcX = currentFrame * width;
                srcY = currentFrameY * height;
                abc = abc +1;
            } else if ( abc == 2){
                //srcY = 1 * height;
                srcX = currentFrame * width;
                srcY = currentFrameY * height;
                abc = abc +1;
            } else if ( abc == 3){
                //srcY = 1 * height;
                srcX = currentFrame * width;
                srcY = currentFrameY * height;
                abc = abc +1;
            } else if ( abc == 4){
                //srcY = 1 * height;
                srcX = currentFrame * width;
                srcY = currentFrameY * height;
                abc = abc +1;
            } else if ( abc == 5){
                //srcY = 1 * height;
                srcX = currentFrame * width;
                srcY = currentFrameY * height;
                abc = abc +1;
            } else if ( abc == 6){
                //srcY = 1 * height;
                srcX = currentFrame * width;
                srcY = currentFrameY * height;
                abc = abc +1;
            } else if ( abc == 7){
                //srcY = 1 * height;
                srcX = currentFrame * width;
                srcY = currentFrameY * height;
                abc = abc +1;
            } else if ( abc == 8){
                //srcY = 1 * height;
                srcX = currentFrame * width;
                srcY = currentFrameY * height;
                abc = abc +1;
            }

            Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
            //Rect dst = new Rect(x, (int)ownY, x + width, (int)ownY + height);
            Rect dst = new Rect(x, y, x + width, y + height);
            //Rect dst = new Rect(x, 150, x + width, 150 + height);
            canvas.drawBitmap(bitmap, src, dst, null);

            if ( abc == 9){
                abc = 0;
            }
        /*} else if(GamePanel.isMoveUpDownGameOver && directionButtons.getMoveUpYTotalHeightAfterGameOver() == 510){
            canvas.drawBitmap(bitmap, getGunX(), getGunY(), null);
        }*/


    }

    public int getDiverHeightSpriteSheet(){
        return this.height;
    }

    public int getDiverWidthSpriteSheet(){
        return this.width;
    }

    public void setGunY(int y){
        this.y = y;
    }

    public int getGunY(){
        return this.y;
    }

    public void setGunX(int x){
        this.x = x;
    }

    public int getGunX(){
        return this.x;
    }

}
