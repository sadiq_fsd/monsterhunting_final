package com.google.android.gms.example.movebackgroundimage;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;


public class Game extends Activity {
    InterstitialAd mInterstitialAd;

    public static int SCREEN_WIDTH =0;
    public static int SCREEN_HEIGHT =0;
    public static Vibrator vibrator;

    //public static MediaPlayer mediaPlayerFire, mediaPlayerBackground;//mediaPlayer//
    GamePanel gamePanel;

    //boolean checkCanvasLockOrUnlock = false;

    @Override
    protected void onPause() {
        super.onPause();
        ClickToPaly.backGroundSound.stop();

        //gamePanel.setPauseThread();

        /*if ( !checkCanvasLockOrUnlock  ){
            gamePanel.surfaceHolderLockCanvas();
            checkCanvasLockOrUnlock = true;
        }*/

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        screenResolution();
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        gamePanel = new GamePanel(this);
        //show interestial ads when game over
        gamePanel.setOnGameOverListener(new GamePanel.GameOverInterface() {
            @Override
            public void gameOver() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mInterstitialAd = new InterstitialAd(Game.this);
                        // set the ad unit ID
                        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));
                        AdRequest adRequest = new AdRequest.Builder().build();
                        // Load ads into Interstitial Ads
                        mInterstitialAd.loadAd(adRequest);
                        mInterstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                            }

                            @Override
                            public void onAdFailedToLoad(int errorCode) {
                                super.onAdFailedToLoad(errorCode);
                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            public void onAdLoaded() {
                                if (mInterstitialAd.isLoaded()) {
                                    mInterstitialAd.show();
                                }
                            }
                        });
                    }
                });
            }
        });
        setContentView(gamePanel);
        //setContentView(new GamePanel(this));
    }

    private void screenResolution(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        SCREEN_WIDTH = size.x;
        SCREEN_HEIGHT = size.y;
    }

    @Override
    protected void onResume() {
        super.onResume();

        gamePanel.startThread();
        try {
            ClickToPaly.backGroundSound.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ClickToPaly.backGroundSound = new MediaPlayer();
        ClickToPaly.backGroundSound = ClickToPaly.backGroundSound.create(this, R.raw.background);
        new android.os.Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                ClickToPaly.backGroundSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        ClickToPaly.backGroundSound.start();
                    }
                });
                ClickToPaly.backGroundSound.start();
            }
        }, 3000);

        /*ClickToPaly.backGroundSound.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {

                Log.e("mEDIApLAYERStatus", "onError : What : " + " , Extra : " + extra);
                Log.e("mEDIApLAYERStatus", "");
                return false;
            }
        });

        ClickToPaly.backGroundSound.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                Log.e("mEDIApLAYERStatus", "onInfo : What : " + " , Extra : " + extra);
                return false;
            }
        });*/


        /*gamePanel.draw(MainThread.canvas);
        gamePanel.surfaceHolderUnLockCanvas();*/
        //gamePanel.setUnPauseThread();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
}
