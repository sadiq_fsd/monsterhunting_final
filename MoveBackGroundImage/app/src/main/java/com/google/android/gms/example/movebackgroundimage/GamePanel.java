package com.google.android.gms.example.movebackgroundimage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

import BackGrounds.WaterBackGroudOne;
import BulletPack.BulletsPack;
import BulletsClasses.BulletEight;
import BulletsClasses.BulletEighteen;
import BulletsClasses.BulletEleven;
import BulletsClasses.BulletFifteen;
import BulletsClasses.BulletFive;
import BulletsClasses.BulletFour;
import BulletsClasses.BulletFourteen;
import BulletsClasses.BulletNew;
import BulletsClasses.BulletNine;
import BulletsClasses.BulletNineteen;
import BulletsClasses.BulletOne;
import BulletsClasses.BulletSeven;
import BulletsClasses.BulletSeventeen;
import BulletsClasses.BulletSix;
import BulletsClasses.BulletSixteen;
import BulletsClasses.BulletTen;
import BulletsClasses.BulletThirteen;
import BulletsClasses.BulletThree;
import BulletsClasses.BulletTwelve;
import BulletsClasses.BulletTwenty;
import BulletsClasses.BulletTwo;
import ButtonFire.FireButton;
import CustomDialogBox.DialogBoxCustom;
import DatabaseFolder.Dbhandler;
import DirectionPad.DirectionButtons;
import DirectionPad.DirectionDown;
import DirectionPad.DirectionLeft;
import DirectionPad.DirectionRight;
import LifePack.LifePacks;
import MySpriteSheet.HammerheadOneSprite;
import MySpriteSheet.HammerheadSprite;
import MySpriteSheet.SharkSmallSprite;
import MySpriteSheet.Sprite;
import ObjectsClasses.ScoreRecord;
import TotalLifeChance.LifeChance;

/**
 * Created by sadiq on 9/7/2016.
 */
public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {

    ScoreRecord scoreRecord;
    Dbhandler dbhandler;

    public static final int WIDTH = Game.SCREEN_WIDTH;
    ;//1024;//
    public static final int HEIGHT = Game.SCREEN_HEIGHT;
    ;//600;//

    public static int scoreValue = 0;
    public static int totalChance = 50;
    public static int bulletCount = 0;

    public static int killAllEnemyFireCount = 0;
    public static int killSmallSharkFireCount = 0;
    public static int killHammerHeadOneFireCount = 0;
    public static int killHammerHeadFireCount = 0;
    public static int killAngularFireCount = 0;

    boolean isSoundChanges = false;
    public static boolean isSaveInDatabaseOnGameOver = true;

    public static boolean isFired = false;
    public static boolean isMoveUpDown = false;
    public static boolean isMoveUpDownGameOver = false;
    public static boolean isMoveRight = false;
    public static boolean isMoveLeft = false;

    public static int fireButtonHeight = 0;
    private Sprite sprite;
    private SharkSmallSprite sharkSmallSprite;
    //private SmallAnglarFishSprite smallAnglarFishSprite;
    private HammerheadSprite hammerheadSprite;
    private HammerheadOneSprite hammerheadOneSprite;
    private Bitmap blob;
    private Bitmap sharkSmallSpriteBitmap;
    //private Bitmap smallAnglarFishSpriteBitmap;
    private Bitmap hammerheadSpriteBitmap;
    private Bitmap hammerheadOneSpriteBitmap;

    private BulletsPack bulletsPack;
    private Bitmap bulletsPackBitmap;

    private int randomX = 0;
    private int randomY = 0;
    private int randomBullets = 0;

    private int currentDiverXPsition = 0;
    private int currentDiverYPsition = 0;

    private boolean soundBoolean = false;

    private Paint textPaint;

    int scaledSizeText = 0;

    private int touchX[];
    private int touchY[];

    private boolean boolIsFired = false;

    private DialogBoxCustom gButton, gameOverPopUpBackground, gameOverPopUpCancelButton, gameOverPopUpGameOverSpell;//gButton1 //for dialog box
    public MainThread thread;
    private Background bg;
    private Gun gun;

    private FireButton fireButton;
    private Bitmap fireButtonBitmap;

    private WaterBackGroudOne waterBackGroudOne;
    private WaterBackGroudOne waterBackGroudTwo;
    private WaterBackGroudOne waterBackGroudThree;

    private Bitmap waterBackGroundOneBitmap;
    private Bitmap waterBackGroundOneBitmapTwo;
    private Bitmap waterBackGroundOneBitmapThree;


    private Bitmap btnBitMap;//for dialog box
    private Bitmap gameOverPopUpBackgroundBitmap;
    private Bitmap gameOverPopUpCancelButtonBitmap;
    private Bitmap gameOverPopUpGameOverSpellBitmap;

    /*private TargetBird targetBird;
    private TargetBirdOne targetBirdOne;
    private Bitmap targetBirdBitMap;
    private Bitmap targetBirdOneBitmap;*/

    //private ScoreImage scoreImage;
    //private Bitmap fireCounterBitmap;

    private LifeChance lifeChanceOne;
    private LifeChance lifeChanceTwo;
    private LifeChance lifeChanceThree;
    private LifeChance lifeChanceFour;

    private LifeChance lifeChanceOneBack;
    private LifeChance lifeChanceTwoBack;
    private LifeChance lifeChanceThreeBack;
    private LifeChance lifeChanceFourBack;

    private Bitmap lifeChanceBitmapOneFront;
    private Bitmap lifeChanceBitmapTwoFront;
    private Bitmap lifeChanceBitmapThreeFront;
    private Bitmap lifeChanceBitmapFourFront;

    private Bitmap lifeChanceBitmapOneBackGround;
    private Bitmap lifeChanceBitmapTwoBackGround;
    private Bitmap lifeChanceBitmapThreeBackGround;
    private Bitmap lifeChanceBitmapFourBackGround;

    private LifePacks lifePacks;
    private Bitmap lifePacksBitmap;

    //private Bitmap directionButtonsPlusBitmap;
    //private Bitmap directionButtonsCircleBitmap;

    private DirectionLeft moveLeft;
    private DirectionRight moveRight;
    private DirectionButtons moveUp;
    private DirectionDown moveDown;


    private Bitmap directionButtonsBitmapLeftOne;
    private Bitmap directionButtonsBitmapRightOne;
    private Bitmap directionButtonsBitmapUpOne;
    private Bitmap directionButtonsBitmapDownOne;

    //=============

    public static BulletNew newBulletnew;
    public static BulletOne bulletOne;
    public static BulletTwo bulletTwo;
    public static BulletThree bulletThree;
    public static BulletFour bulletFour;
    public static BulletFive bulletFive;
    public static BulletSix bulletSix;
    public static BulletSeven bulletSeven;
    public static BulletEight bulletEight;
    public static BulletNine bulletNine;
    public static BulletTen bulletTen;
    public static BulletEleven bulletEleven;
    public static BulletTwelve bulletTwelve;
    public static BulletThirteen bulletThirteen;
    public static BulletFourteen bulletFourteen;
    public static BulletFifteen bulletFifteen;
    public static BulletSixteen bulletSixteen;
    public static BulletSeventeen bulletSeventeen;
    public static BulletEighteen bulletEighteen;
    public static BulletNineteen bulletNineteen;
    public static BulletTwenty bulletTwenty;

    //======
    //private BulletNew bulletNew = null; //0
    private Bitmap bulletBitMap;
    //private BulletOne bulletOneNew = null; //1
    private Bitmap bulletOneBitMap;
    //private BulletTwo bulletTwoNew = null; //2
    private Bitmap bulletTwoBitMap;
    //private BulletThree bulletThreeNew = null; //3
    private Bitmap bulletThreeBitMap;
    //private BulletFour bulletFourNew = null; //4
    private Bitmap bulletFourBitMap;
    //private BulletFive bulletFiveNew = null; //5
    private Bitmap bulletFiveBitMap;
    //private BulletSix bulletSixNew = null; //6
    private Bitmap bulletSixBitMap;
    //private BulletSeven bulletSevenNew = null; //7
    private Bitmap bulletSevenBitMap;
    //private BulletEight bulletEightNew = null; //8
    private Bitmap bulletEightBitMap;
    //private BulletNine bulletNineNew = null; //9
    private Bitmap bulletNineBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletTenBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletElevenBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletTwelveBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletThirteenBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletFourteenBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletFifteenBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletSixteenBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletSeventeenBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletEighteenBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletNineteenBitMap;
    //private BulletNine bulletTenNew = null; //9
    private Bitmap bulletTwentyBitMap;

    Context context;

    GameOverInterface gameOverListener;
    boolean GameOverOneTime = true;

    //Typeface plain, bold;
    //Paint customPaint;
    //AssetManager assetManager;

    SurfaceHolder surfaceHolder;



    public interface GameOverInterface {
        public void gameOver();
    }

    public void setOnGameOverListener(GameOverInterface gameOverListener){
        this.gameOverListener = gameOverListener;
    }


    public GamePanel(Context context) {
        super(context);

        this.context = context;
        dbhandler = new Dbhandler(context);
        getHolder().addCallback(this);
        textPaint = new Paint();
        //scoreFont = new Paint();
        scaledSizeText = getResources().getDimensionPixelSize(R.dimen.myFontSize);
        surfaceHolder = getHolder();
        //gameAds = new Game();
        //customPaint = new Paint();
        setFocusable(true);
    }

    public void startThread() {
        thread = new MainThread(getHolder(), this);
    }

   /* public void setPauseThread() {
        if ( thread.getRunning()){
            thread.setRunning(false);
        }
    }*/

    /*public void setUnPauseThread() {
        if ( !thread.getRunning()){

            thread.setRunning(true);
            *//*new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    getHolder().unlockCanvasAndPost(MainThread.canvas);
                }
            }, 3000);*//*
        }
    }*/

    /*public void surfaceHolderLockCanvas() {
        MainThread.canvas = getHolder().lockCanvas();
    }

    public boolean surfaceHolderUnLockCanvas() {
        getHolder().unlockCanvasAndPost(MainThread.canvas);

        return true;
    }*/

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        create();
        thread.setRunning(true);
        try {
            thread.start();
        } catch (Exception e) {
            if (!thread.isAlive()) {
                thread.start();
            }
//            thread.setRunning(thread.isInterrupted());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                thread.setRunning(false);
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            retry = false;
        }
    }

    public int getRandomValueX() {
        int minX = GamePanel.WIDTH;
        int maxX = GamePanel.WIDTH + 300;

        Random rX = new Random();
        //randomX = rX.nextInt(maxX - minX + 1) + minX;
        randomX = rX.nextInt(maxX - minX) + minX;
        return randomX;
    }

    public int getRandomValue() {
        //int minY = 55;
        int minY = 1;
        int maxY = GamePanel.HEIGHT - GamePanel.fireButtonHeight - sprite.getHeightSpriteSheet();
        //int maxY = GamePanel.HEIGHT - GamePanel.fireButtonHeight - 200;
        //int maxY = GamePanel.HEIGHT;

        Random r = new Random();
        //randomY = r.nextInt(maxY - minY + 1) + minY;
        randomY = r.nextInt(maxY - minY) + minY;
        return randomY;
    }

    public int getRandomBulletsChance() {
        int minBullets = 5;
        int maxBullets = 10;
        //int maxY = GamePanel.HEIGHT;

        Random r = new Random();
        //randomY = r.nextInt(maxY - minY + 1) + minY;
        randomBullets = r.nextInt(maxBullets - minBullets) + minBullets;
        return randomBullets;
    }

    public void create() {

        directionButtonsBitmapLeftOne = BitmapFactory.decodeResource(getResources(), R.drawable.left_1);
        directionButtonsBitmapRightOne = BitmapFactory.decodeResource(getResources(), R.drawable.right_1);
        directionButtonsBitmapUpOne = BitmapFactory.decodeResource(getResources(), R.drawable.up_1);
        directionButtonsBitmapDownOne = BitmapFactory.decodeResource(getResources(), R.drawable.down_1);

        //directionButtonsPlusBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.plus_handle);
        //directionButtonsCircleBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.circle_handel);

        lifePacksBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.get_life);
        //fireCounterBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.fire_counter);

        lifeChanceBitmapOneFront = BitmapFactory.decodeResource(getResources(), R.drawable.life);
        lifeChanceBitmapTwoFront = BitmapFactory.decodeResource(getResources(), R.drawable.life);
        lifeChanceBitmapThreeFront = BitmapFactory.decodeResource(getResources(), R.drawable.life);
        lifeChanceBitmapFourFront = BitmapFactory.decodeResource(getResources(), R.drawable.life);

        lifeChanceBitmapOneBackGround = BitmapFactory.decodeResource(getResources(), R.drawable.life_1);
        lifeChanceBitmapTwoBackGround = BitmapFactory.decodeResource(getResources(), R.drawable.life_1);
        lifeChanceBitmapThreeBackGround = BitmapFactory.decodeResource(getResources(), R.drawable.life_1);
        lifeChanceBitmapFourBackGround = BitmapFactory.decodeResource(getResources(), R.drawable.life_1);

        bulletsPackBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.get_fire);

        btnBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.play_again);//for dialog box
        gameOverPopUpBackgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.button_background);//for dialog box
        gameOverPopUpCancelButtonBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cancel);//for dialog box
        gameOverPopUpGameOverSpellBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.game_over);//for dialog box

        //targetBirdBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.twitter_circle_gray);
        //targetBirdOneBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.twitter_circle_gray);//blackstyle_home

        blob = BitmapFactory.decodeResource(getResources(), R.drawable.killer_whale_main);//bad1
        //blob = BitmapFactory.decodeResource(getResources(), R.drawable.killer_whale);//bad1

        sharkSmallSpriteBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.shark_small);//bad1
        //smallAnglarFishSpriteBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.small_anglar_fish);//bad1
        hammerheadSpriteBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.hammerhead);//bad1

        hammerheadOneSpriteBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.hammerhead_1);//bad1
        //hammerheadOneSpriteBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.hammerhead_2);//bad1

        fireButtonBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.fire_btn);

        waterBackGroundOneBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.new_bg_1);
        waterBackGroundOneBitmapTwo = BitmapFactory.decodeResource(getResources(), R.drawable.new_bg_2);
        waterBackGroundOneBitmapThree = BitmapFactory.decodeResource(getResources(), R.drawable.new_bg_3);

        waterBackGroudOne = new WaterBackGroudOne(waterBackGroundOneBitmap, 0, 0, 2);
        waterBackGroudTwo = new WaterBackGroudOne(waterBackGroundOneBitmapTwo, 0, 0, 4);
        waterBackGroudThree = new WaterBackGroudOne(waterBackGroundOneBitmapThree, 0, 0, 6);

        bg = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.bg_image));
        bg.setVector(-1);

        /**
         * commment by sadiq 11-28-2016
         *
         * sprite = new Sprite(this, blob, (int) (GamePanel.WIDTH /11.96), 0 );//GamePanel.HEIGHT/2
         sharkSmallSprite = new SharkSmallSprite(this, sharkSmallSpriteBitmap, (int) (GamePanel.WIDTH /11.96), (GamePanel.HEIGHT - getRandomValue()));// 102.30 //GamePanel.HEIGHT/2
         smallAnglarFishSprite = new SmallAnglarFishSprite(this, smallAnglarFishSpriteBitmap, (int) (GamePanel.WIDTH /11.96),(GamePanel.HEIGHT - getRandomValue()) );//(GamePanel.HEIGHT - getRandomValue())  //GamePanel.HEIGHT/2
         hammerheadSprite = new HammerheadSprite(this, hammerheadSpriteBitmap, (int) (GamePanel.WIDTH /11.96), (GamePanel.HEIGHT - getRandomValue()) );//(GamePanel.HEIGHT - getRandomValue()) //GamePanel.HEIGHT/2
         hammerheadOneSprite = new HammerheadOneSprite(this, hammerheadOneSpriteBitmap, (int) (GamePanel.WIDTH /11.96), (GamePanel.HEIGHT - getRandomValue()) );//(GamePanel.HEIGHT - getRandomValue() )  //GamePanel.HEIGHT/2

         bulletsPack = new BulletsPack(this, bulletsPackBitmap, GamePanel.WIDTH + 150, GamePanel.HEIGHT/4 );
         lifePacks = new LifePacks( this, lifePacksBitmap, (GamePanel.WIDTH), 300);//100
         */

        //sprite = new Sprite(this, blob, (int) -(GamePanel.WIDTH /5.98), 0 );//GamePanel.HEIGHT/2,  200
        sprite = new Sprite(this, blob, (int) -(GamePanel.WIDTH / 3.987), 0);//GamePanel.HEIGHT/2,  300
        sharkSmallSprite = new SharkSmallSprite(this, sharkSmallSpriteBitmap, (int) -(GamePanel.WIDTH / 5.98), (GamePanel.HEIGHT - getRandomValue()));// 102.30 //GamePanel.HEIGHT/2
        //smallAnglarFishSprite = new SmallAnglarFishSprite(this, smallAnglarFishSpriteBitmap, (int) -(GamePanel.WIDTH /5.98),(GamePanel.HEIGHT - getRandomValue()) );//(GamePanel.HEIGHT - getRandomValue())  //GamePanel.HEIGHT/2
        hammerheadSprite = new HammerheadSprite(this, hammerheadSpriteBitmap, (int) -(GamePanel.WIDTH / 5.98), (GamePanel.HEIGHT - getRandomValue()));//(GamePanel.HEIGHT - getRandomValue()) //GamePanel.HEIGHT/2
        hammerheadOneSprite = new HammerheadOneSprite(this, hammerheadOneSpriteBitmap, (int) -(GamePanel.WIDTH / 5.98), (GamePanel.HEIGHT - getRandomValue()));//(GamePanel.HEIGHT - getRandomValue() )  //GamePanel.HEIGHT/2

        bulletsPack = new BulletsPack(this, bulletsPackBitmap, GamePanel.WIDTH + 150, GamePanel.HEIGHT / 4);
        lifePacks = new LifePacks(this, lifePacksBitmap, (GamePanel.WIDTH), 300);//100

        //player = new Player(BitmapFactory.decodeResource(getResources(), R.drawable.twitter_circle_gray), 64, 64, 1);
        //gun = new Gun(this, BitmapFactory.decodeResource(getResources(), R.drawable.divver_img_color), 10 , (GamePanel.HEIGHT/2), sprite, bulletsPack, lifePacks);

        //gun = new Gun(this, BitmapFactory.decodeResource(getResources(), R.drawable.diver_main), 10 , (GamePanel.HEIGHT/2), sprite, bulletsPack, lifePacks, hammerheadOneSprite, sharkSmallSprite, smallAnglarFishSprite, hammerheadSprite);

        /**
         * chalti hui coding
         * commment by sadiq 11-28-2016
         * gun = new Gun(this, BitmapFactory.decodeResource(getResources(), R.drawable.diver_main), (int) ((GamePanel.WIDTH/2.5)+(GamePanel.WIDTH/5.98)), (GamePanel.HEIGHT/2),
         sprite, bulletsPack, lifePacks, hammerheadOneSprite, sharkSmallSprite, smallAnglarFishSprite, hammerheadSprite);*/

        /*if ( isMoveUpDownGameOver ){ // just single image
            gun = new Gun(this, BitmapFactory.decodeResource(getResources(), R.drawable.diver_die), (int) (GamePanel.WIDTH/59.8), (GamePanel.HEIGHT/2),
                    sprite, bulletsPack, lifePacks, hammerheadOneSprite, sharkSmallSprite, smallAnglarFishSprite, hammerheadSprite);
        } else if ( !isMoveUpDownGameOver ){ //run sprite sheet*/
            /*gun = new Gun(this, BitmapFactory.decodeResource(getResources(), R.drawable.diver_main), (int) ((GamePanel.WIDTH/2) - (GamePanel.WIDTH/2.6578)), (GamePanel.HEIGHT/2),
                    sprite, bulletsPack, lifePacks, hammerheadOneSprite, sharkSmallSprite, smallAnglarFishSprite, hammerheadSprite);*/
        gun = new Gun(this, BitmapFactory.decodeResource(getResources(), R.drawable.diver_main), (int) ((GamePanel.WIDTH / 2) - (GamePanel.WIDTH / 2.6578)), (GamePanel.HEIGHT / 2),
                sprite, bulletsPack, lifePacks, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//, smallAnglarFishSprite
        /*}*/


        /*gun = new Gun(this, BitmapFactory.decodeResource(getResources(), R.drawable.divver_img_color), (int) (GamePanel.WIDTH/2.5), (GamePanel.HEIGHT/2),
                sprite, bulletsPack, lifePacks, hammerheadOneSprite, sharkSmallSprite, smallAnglarFishSprite, hammerheadSprite);*/

        //targetBird = new TargetBird(this, targetBirdBitMap, (bg.getTotalWidth() - 74 ), 0);
        //targetBirdOne = new TargetBirdOne(this, targetBirdOneBitmap, (bg.getTotalWidth() - 74 ), 500 );

        gButton = new DialogBoxCustom(100, 50, btnBitMap, (int) (GamePanel.WIDTH / 2.902), (int) (GamePanel.HEIGHT / 1.8));//412, 375 +20
        //gButton = new DialogBoxCustom(100, 50, btnBitMap, (int) (GamePanel.WIDTH / 2.902), (int) (GamePanel.HEIGHT / 1.822));//412, 375 +20

        //gButton = new DialogBoxCustom( 100, 50, btnBitMap, (int) (GamePanel.WIDTH/2.5), (int) (GamePanel.HEIGHT/2.4));//  y = 300  for dialog box
        //gButton1 = new DialogBoxCustom(100, 50, btnBitMap, (int) (GamePanel.WIDTH/1.8), (int) (GamePanel.HEIGHT/2.4));//  y = 300  for dialog box
        gameOverPopUpBackground = new DialogBoxCustom(100, 50, gameOverPopUpBackgroundBitmap, (int) (GamePanel.WIDTH / 3.88), (int) (GamePanel.HEIGHT / 3.75));//  y = 300  for dialog box
        //gameOverPopUpBackground = new DialogBoxCustom(100, 50, gameOverPopUpBackgroundBitmap, (int) (GamePanel.WIDTH / 3.88), (int) (GamePanel.HEIGHT / 3.823));//  y = 300  for dialog box
        gameOverPopUpCancelButton = new DialogBoxCustom(50, 50, gameOverPopUpCancelButtonBitmap, (GamePanel.WIDTH - (int) (GamePanel.WIDTH / 2.931)), ((int) (GamePanel.HEIGHT / 4.8)));//408,150
        //gameOverPopUpGameOverSpell = new DialogBoxCustom(50, 50, gameOverPopUpGameOverSpellBitmap, (int) (GamePanel.WIDTH / 3.106), (int) (GamePanel.HEIGHT / 3.130));//  385,230 // 2.88 =250
        gameOverPopUpGameOverSpell = new DialogBoxCustom(50, 50, gameOverPopUpGameOverSpellBitmap, (int) (GamePanel.WIDTH / 3.106), (int) (GamePanel.HEIGHT / 3.2));//  385,230 // 2.88 =250

        fireButton = new FireButton(this, fireButtonBitmap, (GamePanel.WIDTH - fireButtonBitmap.getWidth()) - 25, (GamePanel.HEIGHT - fireButtonBitmap.getHeight()) - 30);

        //scoreImage = new ScoreImage(this, fireCounterBitmap, GamePanel.WIDTH/2, 0);
        lifeChanceOne = new LifeChance(this, lifeChanceBitmapOneFront, (GamePanel.WIDTH / 118), (int) (GamePanel.HEIGHT / 70.9));//10
        lifeChanceTwo = new LifeChance(this, lifeChanceBitmapTwoFront, (int) (GamePanel.WIDTH / 29.8), (int) (GamePanel.HEIGHT / 70.9));//40
        lifeChanceThree = new LifeChance(this, lifeChanceBitmapThreeFront, (int) (GamePanel.WIDTH / 17.085), (int) (GamePanel.HEIGHT / 70.9));//70
        lifeChanceFour = new LifeChance(this, lifeChanceBitmapFourFront, (int) (GamePanel.WIDTH / 11.93), (int) (GamePanel.HEIGHT / 70.9));//100

        lifeChanceOneBack = new LifeChance(this, lifeChanceBitmapOneBackGround, (GamePanel.WIDTH / 118), (int) (GamePanel.HEIGHT / 70.9));//10
        lifeChanceTwoBack = new LifeChance(this, lifeChanceBitmapTwoBackGround, (int) (GamePanel.WIDTH / 29.8), (int) (GamePanel.HEIGHT / 70.9));//40
        lifeChanceThreeBack = new LifeChance(this, lifeChanceBitmapThreeBackGround, (int) (GamePanel.WIDTH / 17.085), (int) (GamePanel.HEIGHT / 70.9));//70
        lifeChanceFourBack = new LifeChance(this, lifeChanceBitmapFourBackGround, (int) (GamePanel.WIDTH / 11.93), (int) (GamePanel.HEIGHT / 70.9));//100

        moveDown = new DirectionDown(this, directionButtonsBitmapDownOne, (int) (GamePanel.WIDTH / 13.288), ((GamePanel.HEIGHT - directionButtonsBitmapDownOne.getHeight()) - 15), gun);//90 ,
        moveUp = new DirectionButtons(this, directionButtonsBitmapUpOne, (int) (GamePanel.WIDTH / 13.288), (int) (GamePanel.HEIGHT - (GamePanel.HEIGHT / 3.17)), gun);//90, 227
        moveLeft = new DirectionLeft(this, directionButtonsBitmapLeftOne, (GamePanel.WIDTH / 59), (int) (GamePanel.HEIGHT - (GamePanel.HEIGHT / 4.5)), gun);//20,160
        moveRight = new DirectionRight(this, directionButtonsBitmapRightOne, (int) (GamePanel.WIDTH / 8.729), (int) (GamePanel.HEIGHT - (GamePanel.HEIGHT / 4.5)), gun);//137, 160
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getActionMasked();//event.getAction() & MotionEvent.ACTION_MASK;
        int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
        int pointerID = event.getPointerId(pointerIndex);//maybe pointer id detect multiple touch on screen
        int pointerCount = event.getPointerCount();

        touchX = new int[pointerCount];
        touchY = new int[pointerCount];

        for (int i = 0; i < pointerCount; i++) {
            touchX[i] = (int) event.getX(i);
            touchY[i] = (int) event.getY(i);
        }

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (!DialogBoxCustom.isGameOver) {
                    //for game not over
                    bulletBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow); //0
                    bulletOneBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow); //1
                    bulletTwoBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//2
                    bulletThreeBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//3
                    bulletFourBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//4
                    bulletFiveBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//5
                    bulletSixBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//6
                    bulletSevenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//7
                    bulletEightBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//8
                    bulletNineBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletTenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletElevenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletTwelveBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletThirteenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletFourteenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletFifteenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletSixteenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletSeventeenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletEighteenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletNineteenBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9
                    bulletTwentyBitMap = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);//9

                    //click on fire button_close
                    if (touchX[0] > (GamePanel.WIDTH - fireButtonBitmap.getWidth()) && touchX[0] < (GamePanel.WIDTH)
                            && touchY[0] >= (GamePanel.HEIGHT - fireButton.getGireButtonHeight()) && touchY[0] < GamePanel.HEIGHT
                            //&& this.bulletCount < 50
                            && (this.totalChance <= 50 && this.totalChance >= 1)
                            ) {
                        if (!boolIsFired) { //not true

                            boolIsFired = true;//for if condition only
                            setBulletFired(true);//GamePanel.isFired
                            //ClickToPaly.mediaPlayerFire.start();
                            this.multiFireCase();

                            this.bulletCount += 1;
                            boolIsFired = false;
                        }
                    }

                    if ( //left click
                        //touchX[0] > (GamePanel.WIDTH/59 ) && touchX[0] < (GamePanel.WIDTH/8.729 ) //20, 137
                            touchX[0] > (GamePanel.WIDTH / 59) && touchX[0] < (GamePanel.WIDTH / 11.96) //20, 60
                                    && touchY[0] >= (GamePanel.HEIGHT - (GamePanel.HEIGHT / 4.5))
                                    && touchY[0] < GamePanel.HEIGHT - (GamePanel.HEIGHT / 11.076)//160, 65
                            ) {
                        isMoveLeft = true;
                        moveUp.setMoveLeftRightBoolean(true);
                        return true;
                    } else if (//right click
                        //touchX[0] > (GamePanel.WIDTH/8.729 ) && touchX[0] < (GamePanel.WIDTH/4.784 ) //137, 250
                            touchX[0] > (GamePanel.WIDTH / 7.035) && touchX[0] < (GamePanel.WIDTH / 4.784) //170, 250
                                    && touchY[0] >= (GamePanel.HEIGHT - (GamePanel.HEIGHT / 4.5))
                                    && touchY[0] < GamePanel.HEIGHT - (GamePanel.HEIGHT / 11.076)   //160, 65
                            ) {
                        isMoveRight = true;
                        moveUp.setMoveLeftRightBoolean(true);

                        return true;
                    } else if (//up click
                            touchX[0] > (int) (GamePanel.WIDTH / 13.288) && touchX[0] < (GamePanel.WIDTH / 6.294) //90, 190
                                    //&& touchY[0] >= (GamePanel.HEIGHT -(GamePanel.HEIGHT/3.17))  && touchY[0] < GamePanel.HEIGHT  - 150   //227, 65
                                    && touchY[0] >= (GamePanel.HEIGHT - (GamePanel.HEIGHT / 3.17))
                                    && touchY[0] < (GamePanel.HEIGHT - (GamePanel.HEIGHT / 4.5))   //227, 160
                            ) {
                        isMoveUpDown = true;
                        moveUp.setMoveUpBoolean(true);
                        return true;
                    } else if (//down click
                            touchX[0] > (int) (GamePanel.WIDTH / 13.288) && touchX[0] < (GamePanel.WIDTH / 4.784) //137, 250
                                    //&& touchY[0] >= (GamePanel.HEIGHT -( GamePanel.HEIGHT/4.5 ) )  && touchY[0] < GamePanel.HEIGHT  - (GamePanel.HEIGHT/11.076 )   //160, 65
                                    && touchY[0] >= GamePanel.HEIGHT - (GamePanel.HEIGHT / 11.076)
                                    && touchY[0] < GamePanel.HEIGHT - 5 //65 , 705
                            ) {
                        fireButtonHeight = fireButton.getGireButtonHeight();
                        isMoveUpDown = true;
                        moveUp.setMoveUpBoolean(false);
                        return true;
                    }

                }//!DialogBoxCustom.isGameOver (if bracket)
                //else if ( DialogBoxCustom.isGameOver && GamePanel.totalChance == 0 ){
                else if (DialogBoxCustom.isGameOver && Gun.LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER == 20) {

                    //gameAds.adMob(getContext());


                    if (touchX[0] > (GamePanel.WIDTH - GamePanel.WIDTH / 2.931) && touchX[0] < (GamePanel.WIDTH - (GamePanel.WIDTH / 2.931) + gameOverPopUpCancelButton.getWidth())
                            && touchY[0] > (GamePanel.HEIGHT / 4.8) && touchY[0] < ((GamePanel.HEIGHT / 4.8) + gameOverPopUpCancelButton.getHeight())
                            ) {
                        ClickToPaly.backGroundSound.stop();
                        Intent i = new Intent(context, ClickToPaly.class);
                        context.startActivity(i);
                        resetVariables();
                        ((Activity) context).finishAffinity();
                    } else if (touchX[0] > (GamePanel.WIDTH / 2.902) && touchX[0] < (GamePanel.WIDTH / 1.523)//412, 412+373 = 785 = 1.523
                            && touchY[0] > (GamePanel.HEIGHT / 1.92) && touchY[0] < (GamePanel.HEIGHT / 1.518)//375, 474
                            ) {
                        //afterGameOverStartGameSound();

                        create();

                        whenGameStartAgain();
                        soundPlayWhenGameStartAgain();

                    }

                    /**
                     * comment today 11-7-2016 working code*/
                    /*if (touchX[0] > (GamePanel.WIDTH/2.5) && touchX[0] < (GamePanel.WIDTH/2.09) && touchY[0] > (GamePanel.HEIGHT/2.4) && touchY[0] < (GamePanel.HEIGHT/1.8)){
                        Gun.LIFE_COUNT_BOOL_ONE = true;
                        Gun.LIFE_COUNT_BOOL_TWO = true;
                        Gun.LIFE_COUNT_BOOL_THREE = true;
                        Gun.LIFE_COUNT_BOOL_FOUR = true;

                        Gun.LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 0;

                        GamePanel.totalChance = 50;
                        GamePanel.scoreValue = 0;
                        this.bulletCount = 0;
                        this.targetBird.targetBirdBoolean(true);
                        DialogBoxCustom.isGameOver = false;

                        //afterGameOverStartGameSound();
                        soundPlayWhenGameStartAgain();
                    }*/
                } //DialogBoxCustom.isGameOver && BulletNew.totalChance == 0 (else if bracket)
                else {
                    DialogBoxCustom.isGameOver = false;
                }

                break;
            case MotionEvent.ACTION_UP:
                if (!DialogBoxCustom.isGameOver) {//for game over
                    isMoveUpDown = false;
                    isMoveRight = false;
                    isMoveLeft = false;
                    if (touchX[0] > 10 && touchX[0] < 70 && touchY[0] > 300 && touchY[0] < 370) {
                        //bulletNew.update(bulletBitMap);
                        //bulletNew.setBulletFired(false);
                        //setBulletFired(false);
                    }
                }

                break;
        }
        return super.onTouchEvent(event);
    }

    private void whenGameStartAgain() {
        isSaveInDatabaseOnGameOver = true;
        Gun.LIFE_COUNT_BOOL_ONE = true;
        Gun.LIFE_COUNT_BOOL_TWO = true;
        Gun.LIFE_COUNT_BOOL_THREE = true;
        Gun.LIFE_COUNT_BOOL_FOUR = true;

        Gun.LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 0;

        GamePanel.totalChance = 50;
        GamePanel.scoreValue = 0;
        this.bulletCount = 0;
        ///this.targetBird.targetBirdBoolean(true);
        DialogBoxCustom.isGameOver = false;

        GamePanel.killAllEnemyFireCount = 0;
        GamePanel.killSmallSharkFireCount = 0;
        GamePanel.killHammerHeadOneFireCount = 0;
        GamePanel.killHammerHeadFireCount = 0;
        GamePanel.killAngularFireCount = 0;

        GamePanel.isMoveUpDownGameOver = false;

        GameOverOneTime = true;

    }

    private void resetVariables() {
        DialogBoxCustom.isGameOver = false;
        Gun.LIFE_COUNT_BOOL_ONE = true;
        Gun.LIFE_COUNT_BOOL_TWO = true;
        Gun.LIFE_COUNT_BOOL_THREE = true;
        Gun.LIFE_COUNT_BOOL_FOUR = true;
        Gun.LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER = 0;

        GamePanel.totalChance = 50;
        GamePanel.scoreValue = 0;

        this.bulletCount = 0;
    }

    private void soundPlayWhenGameStartAgain() {
        if (isSoundChanges) {
            isSoundChanges = false;
            ClickToPaly.backGroundSound.stop();//backGround SOund stop
            ClickToPaly.backGroundSound = null;
            ClickToPaly.backGroundSound = new MediaPlayer();
            ClickToPaly.backGroundSound = ClickToPaly.backGroundSound.create(getContext(), R.raw.background);
            ClickToPaly.backGroundSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    //mp.start();
                    ClickToPaly.backGroundSound.start();
                }
            });
            ClickToPaly.backGroundSound.start();
        }
    }

    private void soundPlayWhenGameOver() {
        if (!isSoundChanges) {
            isSoundChanges = true;
            /*ClickToPaly.backGroundSound.stop();//backGround SOund stop
            ClickToPaly.backGroundSound = null;
            ClickToPaly.backGroundSound = new MediaPlayer();
            ClickToPaly.backGroundSound = ClickToPaly.backGroundSound.create(getContext(), R.raw.splash_and_game_over);
            ClickToPaly.backGroundSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    //mp.start();
                    ClickToPaly.backGroundSound.start();
                }
            });
            ClickToPaly.backGroundSound.start();*/
        }
    }

    private void multiFireCase() {
        //working here
        currentDiverXPsition = gun.getGunX() + gun.getDiverWidthSpriteSheet();
        currentDiverYPsition = gun.getGunY();
        switch (this.bulletCount) {
            case 0:

                ClickToPaly.mediaPlayerFire.start();
                BulletNew bulletNew = new BulletNew(this, bulletBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                newBulletnew = bulletNew;
                break;

            case 1:

                ClickToPaly.mediaPlayerFire.start();
                BulletOne bulletOneNew = new BulletOne(this, bulletOneBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletOne = bulletOneNew;
                break;

            case 2:

                ClickToPaly.mediaPlayerFire.start();
                BulletTwo bulletTwoNew = new BulletTwo(this, bulletTwoBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletTwo = bulletTwoNew;
                break;

            case 3:

                ClickToPaly.mediaPlayerFire.start();
                BulletThree bulletThreeNew = new BulletThree(this, bulletThreeBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletThree = bulletThreeNew;
                break;

            case 4:

                ClickToPaly.mediaPlayerFire.start();
                BulletFour bulletFourNew = new BulletFour(this, bulletFourBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletFour = bulletFourNew;
                break;

            case 5:

                ClickToPaly.mediaPlayerFire.start();
                BulletFive bulletFiveNew = new BulletFive(this, bulletFiveBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletFive = bulletFiveNew;
                break;

            case 6:

                ClickToPaly.mediaPlayerFire.start();
                BulletSix bulletSixNew = new BulletSix(this, bulletSixBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletSix = bulletSixNew;
                break;

            case 7:

                ClickToPaly.mediaPlayerFire.start();
                BulletSeven bulletSevenNew = new BulletSeven(this, bulletSevenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletSeven = bulletSevenNew;
                break;

            case 8:

                ClickToPaly.mediaPlayerFire.start();
                BulletEight bulletEightNew = new BulletEight(this, bulletEightBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletEight = bulletEightNew;

                break;

            case 9:

                ClickToPaly.mediaPlayerFire.start();
                BulletNine bulletNineNew = new BulletNine(this, bulletNineBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletNine = bulletNineNew;

                break;

            case 10:

                ClickToPaly.mediaPlayerFire.start();
                BulletTen bulletTenNew = new BulletTen(this, bulletTenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletTen = bulletTenNew;
                break;

            case 11:

                ClickToPaly.mediaPlayerFire.start();
                BulletEleven bulletElevenNew = new BulletEleven(this, bulletElevenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletEleven = bulletElevenNew;
                break;

            case 12:

                ClickToPaly.mediaPlayerFire.start();
                BulletTwelve bulletTwelveNew = new BulletTwelve(this, bulletTwelveBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletTwelve = bulletTwelveNew;
                break;

            case 13:

                ClickToPaly.mediaPlayerFire.start();
                BulletThirteen bulletThirteenNew = new BulletThirteen(this, bulletThirteenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletThirteen = bulletThirteenNew;
                break;

            case 14:

                ClickToPaly.mediaPlayerFire.start();
                BulletFourteen bulletFourteenNew = new BulletFourteen(this, bulletFourteenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletFourteen = bulletFourteenNew;
                break;

            case 15:

                ClickToPaly.mediaPlayerFire.start();
                BulletFifteen bulletFifteenNew = new BulletFifteen(this, bulletFifteenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletFifteen = bulletFifteenNew;
                break;

            case 16:

                ClickToPaly.mediaPlayerFire.start();
                BulletSixteen bulletSixteenNew = new BulletSixteen(this, bulletSixteenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletSixteen = bulletSixteenNew;
                break;

            case 17:

                ClickToPaly.mediaPlayerFire.start();
                BulletSeventeen bulletSeventeenNew = new BulletSeventeen(this, bulletSeventeenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletSeventeen = bulletSeventeenNew;
                break;

            case 18:

                ClickToPaly.mediaPlayerFire.start();
                BulletEighteen bulletEighteenNew = new BulletEighteen(this, bulletEighteenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletEighteen = bulletEighteenNew;
                break;

            case 19:

                ClickToPaly.mediaPlayerFire.start();
                BulletNineteen bulletNineteenNew = new BulletNineteen(this, bulletNineteenBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletNineteen = bulletNineteenNew;
                break;

            case 20:

                ClickToPaly.mediaPlayerFire.start();
                BulletTwenty bulletTwentyNew = new BulletTwenty(this, bulletTwentyBitMap, currentDiverXPsition, currentDiverYPsition, sprite, gun, hammerheadOneSprite, sharkSmallSprite, hammerheadSprite);//smallAnglarFishSprite,
                this.bulletTwenty = bulletTwentyNew;
                break;

            default:
                Log.e("SwitchCase", "Default");
                break;
        }
    }

    public void update() {
        bg.update();
        //stoneOne.update();
    }

    @Override
    public void draw(Canvas canvas) {
        final float scaleFactorX = getWidth() / WIDTH;
        final float scaleFactorY = getHeight() / HEIGHT;

        if (canvas != null) {
            final int savedstate = canvas.save();
            canvas.scale(scaleFactorX, scaleFactorY);

            //bg.draw(canvas);

            waterBackGroudOne.draw(canvas);
            waterBackGroudTwo.draw(canvas);
            waterBackGroudThree.draw(canvas);

            //scoreImage.draw(canvas);

            if (this.gun.LIFE_COUNT_BOOL_ONE) {
                lifeChanceOne.draw(canvas);
            } else {
                lifeChanceOneBack.draw(canvas);
            }

            if (this.gun.LIFE_COUNT_BOOL_TWO) {
                lifeChanceTwo.draw(canvas);
            } else {
                lifeChanceTwoBack.draw(canvas);
            }

            if (this.gun.LIFE_COUNT_BOOL_THREE) {
                lifeChanceThree.draw(canvas);
            } else {
                lifeChanceThreeBack.draw(canvas);
            }

            if (this.gun.LIFE_COUNT_BOOL_FOUR) {
                lifeChanceFour.draw(canvas);
            } else {
                lifeChanceFourBack.draw(canvas);
            }

            bulletsPack.draw(canvas);
            lifePacks.draw(canvas);

            sprite.draw(canvas);
            sharkSmallSprite.draw(canvas);
            /*//smallAnglarFishSprite.draw(canvas);*/
            hammerheadSprite.draw(canvas);
            hammerheadOneSprite.draw(canvas);

            //working code 10-6-2016
            //moveUp.draw(canvas);
            //moveDown.draw(canvas);

            fireButton.draw(canvas);

            //player.draw(canvas);
            gun.draw(canvas);

            moveDown.draw(canvas);
            moveLeft.draw(canvas);
            moveRight.draw(canvas);
            moveUp.draw(canvas);

            if (newBulletnew != null) {
                newBulletnew.onDraw(canvas);
            }

            //BulletOne class
            if (this.bulletOne != null) {
                this.bulletOne.onDraw(canvas);
            }

            //BulletTwo Class
            if (this.bulletTwo != null) {
                this.bulletTwo.onDraw(canvas);
            }

            //BulletThree Class
            if (this.bulletThree != null) {
                this.bulletThree.onDraw(canvas);
            }

            //BulletFour Class
            if (this.bulletFour != null) {
                this.bulletFour.onDraw(canvas);
            }

            //BulletFive Class
            if (this.bulletFive != null) {
                this.bulletFive.onDraw(canvas);
            }

            //BulletSix Class
            if (this.bulletSix != null) {
                this.bulletSix.onDraw(canvas);
            }

            //bulletSeven Class
            if (this.bulletSeven != null) {
                this.bulletSeven.onDraw(canvas);
            }

            //BulletEight Class
            if (this.bulletEight != null) {
                this.bulletEight.onDraw(canvas);
            }

            //nine
            if (this.bulletNine != null) {
                this.bulletNine.onDraw(canvas);
            }

            //ten
            if (this.bulletTen != null) {
                this.bulletTen.onDraw(canvas);
            }

            //eleven
            if (this.bulletEleven != null) {
                this.bulletEleven.onDraw(canvas);
            }

            //twelve
            if (this.bulletTwelve != null) {
                this.bulletTwelve.onDraw(canvas);
            }

            //thirteen
            if (this.bulletThirteen != null) {
                this.bulletThirteen.onDraw(canvas);
            }

            //fourteen
            if (this.bulletFourteen != null) {
                this.bulletFourteen.onDraw(canvas);
            }

            //fifteen
            if (this.bulletFifteen != null) {
                this.bulletFifteen.onDraw(canvas);
            }

            //sixteen
            if (this.bulletSixteen != null) {
                this.bulletSixteen.onDraw(canvas);
            }

            //seventeen
            if (this.bulletSeventeen != null) {
                this.bulletSeventeen.onDraw(canvas);
            }

            //eighteen
            if (this.bulletEighteen != null) {
                this.bulletEighteen.onDraw(canvas);
            }

            //nineteen
            if (this.bulletNineteen != null) {
                this.bulletNineteen.onDraw(canvas);
            }

            //twenty
            if (this.bulletTwenty != null) {
                this.bulletTwenty.onDraw(canvas);
            }

            /*//BulletNine Class
            if (this.bulletNine != null ){
                this.bulletNine.onDraw(canvas);
            }*/

            //for dialog box
            if (DialogBoxCustom.isGameOver && Gun.LIFE_COUNTER_ENEMY_COLLISION_FOR_GAMEOVER == 20) {
                /**
                 * ======= 11-28-2016 edit code =============
                 */
                GamePanel.isMoveUpDownGameOver = true;
                moveUp.moveYdownDirectionGameOver();
                /**
                 * ========= 11-28-2016 edit code end============
                 */
                /**
                 * ========= call ad mob============
                 */
                if(GameOverOneTime){
                    GameOverOneTime = false;
                    gameOverListener.gameOver();
                }
                /**
                 * ======= end admob =========
                 */
                soundPlayWhenGameOver();

                textPaint.setColor(Color.RED);//for dialog box
                textPaint.setTextSize(scaledSizeText);//for dialog box

                /*canvas.drawText("My Fire Game ", (float) (GamePanel.WIDTH / 2.5), (float) (GamePanel.HEIGHT / 3.6), textPaint);//for dialog box
                canvas.drawText("Your Game is Over!!! ", (float) (GamePanel.WIDTH / 2.5), (float) (GamePanel.HEIGHT / 2.88), textPaint);//for dialog box*/

                //Log.e("backgroundWH", "background width" + gButton.getWidth());
                //Log.e("backgroundWH", "background Height" + gButton.getHeight());


                //canvas.drawText(ClickToPaly.userName,412,375, textPaint);//412, 375
                //canvas.drawText(String.valueOf(GamePanel.scoreValue), 412, 375, textPaint);//412, 375
                //Log.e("dbCount", " count save record => " + ClickToPaly.getDeviceId());

                if (isSaveInDatabaseOnGameOver) {
                    isSaveInDatabaseOnGameOver = false;
                    scoreRecord = new ScoreRecord(ClickToPaly._userNameBeforeGameStart, GamePanel.scoreValue);
                    dbhandler.addScoreRecord(scoreRecord);
                    Log.e("dbCount", " record from class => " + dbhandler.getScoreRecordCount());
                    scoreRecord = new ScoreRecord(dbhandler.getMaxScoreRecord().get_id(),
                            dbhandler.getMaxScoreRecord().get_name(), dbhandler.getMaxScoreRecord().get_score());
                    Log.e("dbCount", " record from class if => " + scoreRecord.get_score());
                    //int a = scoreRecord.get_score();
                    //drawCustomText(canvas, scoreRecord.get_score());
                }
                //gButton1.draw(canvas);//for dialog box

                gameOverPopUpBackground.draw(canvas);//for dialog box
                gameOverPopUpCancelButton.draw(canvas);//for dialog box
                gButton.draw(canvas);//for dialog box
                gameOverPopUpGameOverSpell.draw(canvas);

                /**
                 * parameters
                 * canvas, string, textSize, whereTo draw
                 */
                //drawCustomText(canvas, "Score", 30, "#A52A2A", 1);
                drawCustomText(canvas, "Hi-Score", getResources().getDimension(R.dimen.textSize), "#A52A2A", 1);
                String totalScore = "";
                totalScore = returnAndManageScoreLength(totalScore, scoreRecord.get_score());
                /*String scoreLength = String.valueOf(scoreRecord.get_score());
                String totalScore = "";
                int len = scoreLength.length();
                switch(len){
                    case 1:
                        totalScore = "000"+scoreRecord.get_score();
                        break;
                    case 2:
                        totalScore = "00"+scoreRecord.get_score();
                        break;
                    case 3:
                        totalScore = "0"+scoreRecord.get_score();
                        break;
                    case 4:
                        totalScore = String.valueOf(scoreRecord.get_score());
                        break;
                }*/

                /**
                 * parameters
                 * canvas, string, textSize, whereTo draw
                 */
                drawCustomText(canvas, totalScore,  getResources().getDimension(R.dimen.textScore), "#B22222", 2);
                /*Typeface plain = Typeface.createFromAsset(context.getAssets(), "fonts/porkys.ttf");
                Typeface bold = Typeface.create(plain, Typeface.BOLD);
                Paint customPaint = new Paint();
                customPaint.setAntiAlias(true);
                customPaint.setTypeface(bold);
                customPaint.setTextSize(60);
                //customPaint.setColor(Color.parseColor("#A52A2A"));
                //canvas.drawText("0000", (float)((GamePanel.WIDTH/4.784)+ (gameOverPopUpBackground.getWidth()/2)), (float) (GamePanel.HEIGHT/2.215), customPaint);//250 , 325
                canvas.drawText(totalScore, (float) ((GamePanel.WIDTH / 4.784) + (gameOverPopUpBackground.getWidth() / 2) - (GamePanel.WIDTH/119.6)), (float) (GamePanel.HEIGHT / 1.894) , customPaint);//250+(gameOverPopUpBackground.getWidth()/2)(-10)(opptional- 30 + 20) , 325 + 55
                */
            }

            textPaint.setColor(Color.WHITE);
            textPaint.setTextSize(scaledSizeText);

            /*canvas.drawText("Your Score : ", (float) (GamePanel.WIDTH / 3.4), GamePanel.HEIGHT - 90, textPaint);//(GamePanel.WIDTH/3.4) = 351
            canvas.drawText(String.valueOf(GamePanel.scoreValue), (GamePanel.WIDTH / 2), GamePanel.HEIGHT - 90, textPaint);*/

            canvas.drawText("Your Score : ", (float) (GamePanel.WIDTH / 3.4), (float) (GamePanel.HEIGHT - (GamePanel.HEIGHT / 7.475)), textPaint);//(GamePanel.WIDTH/3.4) = 351
            canvas.drawText(String.valueOf(GamePanel.scoreValue), (GamePanel.WIDTH / 2), (float) (GamePanel.HEIGHT - (GamePanel.HEIGHT / 7.475)), textPaint); //150

            /*canvas.drawText("Total Bullets : ", (float) (GamePanel.WIDTH/3.4), GamePanel.HEIGHT - 50, textPaint);//(GamePanel.WIDTH/3.4) = 351
            canvas.drawText(String.valueOf(GamePanel.totalChance), (GamePanel.WIDTH / 2 ), GamePanel.HEIGHT - 50, textPaint);*/

            canvas.drawText("Total Bullets : ", (float) (GamePanel.WIDTH / 3.4), (float) (GamePanel.HEIGHT - (GamePanel.HEIGHT / 14.95)), textPaint);//(GamePanel.WIDTH/3.4) = 351
            canvas.drawText(String.valueOf(GamePanel.totalChance), (GamePanel.WIDTH / 2), (float) (GamePanel.HEIGHT - (GamePanel.HEIGHT / 14.95)), textPaint);

            canvas.restoreToCount(savedstate);
        }
    }

    private String returnAndManageScoreLength(String totalScore, int score) {
        String scoreLength = String.valueOf(score);

        int len = scoreLength.length();
        switch (len) {
            case 1:
                totalScore = "000" + scoreRecord.get_score();
                break;
            case 2:
                totalScore = "00" + scoreRecord.get_score();
                break;
            case 3:
                totalScore = "0" + scoreRecord.get_score();
                break;
            case 4:
                totalScore = String.valueOf(scoreRecord.get_score());
                break;
        }
        return totalScore;
    }

    private void drawCustomText(Canvas canvas, String text, float textSize, String textColor, int forDrawCanvas) {
        /*** custom fnots ***/
        /**
         * parameters
         * canvas, string, textSize, whereTo draw
         */
        Typeface plain = Typeface.createFromAsset(context.getAssets(), "fonts/porkys.ttf");
        Typeface bold = Typeface.create(plain, Typeface.BOLD);
        Paint customPaint = new Paint();
        customPaint.setAntiAlias(true);
        customPaint.setTypeface(bold);
        customPaint.setTextSize(textSize);
        //customPaint.setTextSize(getResources().getDimension(R.dimen.textSize));
        customPaint.setColor(Color.parseColor(textColor));
        if (forDrawCanvas == 1) {
            //canvas.drawText("Score", (float) ((GamePanel.WIDTH / 4.784) + (gameOverPopUpBackground.getWidth() / 2) + (GamePanel.WIDTH / 59.8)), (float) (GamePanel.HEIGHT / 2.215), customPaint);//250+20 , 325
            canvas.drawText(text, (float) ((GamePanel.WIDTH / 4.784) + (gameOverPopUpBackground.getWidth() / 2) + (GamePanel.WIDTH / 119.6)), (float) (GamePanel.HEIGHT / 2.182), customPaint);//250+20 , 325
            //canvas.drawText(text, (float) ((GamePanel.WIDTH / 4.784) + (gameOverPopUpBackground.getWidth() / 2) + (GamePanel.WIDTH / 59.8)), (float) (GamePanel.HEIGHT / 2.182), customPaint);//250+20 , 325
        } else if (forDrawCanvas == 2) {
            //canvas.drawText(text, (float) ((GamePanel.WIDTH / 4.784) + (gameOverPopUpBackground.getWidth() / 2) - (GamePanel.WIDTH / 119.6)), (float) (GamePanel.HEIGHT / 1.894), customPaint);//250+(gameOverPopUpBackground.getWidth()/2)(-10)(opptional- 30 + 20) , 325 + 55
            canvas.drawText(text, (float) ((GamePanel.WIDTH / 4.784) + (gameOverPopUpBackground.getWidth() / 2) + (GamePanel.WIDTH / 119.6)), (float) (GamePanel.HEIGHT / 1.894), customPaint);//250+(gameOverPopUpBackground.getWidth()/2)(-10)(opptional- 30 + 20) , 325 + 55
            //canvas.drawText(text, (float) ((GamePanel.WIDTH / 4.784) + (gameOverPopUpBackground.getWidth() / 2) + (GamePanel.WIDTH / 34.177)), (float) (GamePanel.HEIGHT / 1.894), customPaint);//250+(gameOverPopUpBackground.getWidth()/2)(-10)(opptional- 30 + 20) , 325 + 55
        }
        /*** custom fnots ***/
    }

    /**
     * boolean for bullet
     *
     * @param isFired
     */
    public void setBulletFired(boolean isFired) {
        this.isFired = isFired;
    }

    /**
     * backGround GetTotalBackgroudHeight
     */
    public int getBackgroundTotalHeight() {
        return bg.getTotalHeight();
    }

    /**
     * backGround GetTotalBackgroudWidth
     * previous call from background class thanks
     */
    public int getBackgroundTotalWidth() {
        //return bg.getTotalWidth();
        return waterBackGroudOne.getWaterBackGroundOneTotalWidth();
    }
}
