package BackGrounds;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by sadiq on 9/22/2016.
 */
public class WaterBackGroudOne {
    private static final int WATERBG_BMP_ROWS = 1;
    private static final int WATERBG_BMP_COLUMNS = 3;

    private int x = 0;
    private int y = 0;

    private int width;
    private int height;

    private Bitmap image;

    private int mBGFarMoveX = 0;
    private int mBGNearMoveX = 0;
    private int speed = 0;


    //public WaterBackGroudOne(Bitmap res){
    public WaterBackGroudOne(Bitmap res, int x, int y,int speed ){ //-1, 0
        this.image = res;
        this.width = this.image.getWidth();// / WATERBG_BMP_COLUMNS;
        this.height = this.image.getHeight();// / WATERBG_BMP_ROWS;
        this.x = 0;
        this.y = 0;
        this.speed = speed;
    }

    public void draw(Canvas canvas){
        //update();

        // decrement the far background
        mBGFarMoveX = mBGFarMoveX - this.speed;
        // decrement the near background
        mBGNearMoveX = mBGNearMoveX - 4;
        // calculate the wrap factor for matching image draw
        int newFarX = image.getWidth() - (-mBGFarMoveX);
        // if we have scrolled all the way, reset to start


        if (newFarX <= 0) {
            mBGFarMoveX = 0;
            // only need one draw
            canvas.drawBitmap(image, mBGFarMoveX, 0, null);
        } else {
            // need to draw original and wrap

            canvas.drawBitmap(image, mBGFarMoveX, 0, null);
            canvas.drawBitmap(image, newFarX, 0, null);
        }



        /*int srcX = currentFrame * width;
        Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
        Rect dst = new Rect(x, y, x + width, y + height);
        canvas.drawBitmap(image, src, dst, null);*/

    }

    public int getWaterBackGroundOneTotalWidth(){
        return this.width;
    }
}
